/*******************************************************************************
* File Name: sdlogger_1_irq_write.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_sdlogger_1_irq_write_H)
#define CY_ISR_sdlogger_1_irq_write_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void sdlogger_1_irq_write_Start(void);
void sdlogger_1_irq_write_StartEx(cyisraddress address);
void sdlogger_1_irq_write_Stop(void);

CY_ISR_PROTO(sdlogger_1_irq_write_Interrupt);

void sdlogger_1_irq_write_SetVector(cyisraddress address);
cyisraddress sdlogger_1_irq_write_GetVector(void);

void sdlogger_1_irq_write_SetPriority(uint8 priority);
uint8 sdlogger_1_irq_write_GetPriority(void);

void sdlogger_1_irq_write_Enable(void);
uint8 sdlogger_1_irq_write_GetState(void);
void sdlogger_1_irq_write_Disable(void);

void sdlogger_1_irq_write_SetPending(void);
void sdlogger_1_irq_write_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the sdlogger_1_irq_write ISR. */
#define sdlogger_1_irq_write_INTC_VECTOR            ((reg32 *) sdlogger_1_irq_write__INTC_VECT)

/* Address of the sdlogger_1_irq_write ISR priority. */
#define sdlogger_1_irq_write_INTC_PRIOR             ((reg8 *) sdlogger_1_irq_write__INTC_PRIOR_REG)

/* Priority of the sdlogger_1_irq_write interrupt. */
#define sdlogger_1_irq_write_INTC_PRIOR_NUMBER      sdlogger_1_irq_write__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable sdlogger_1_irq_write interrupt. */
#define sdlogger_1_irq_write_INTC_SET_EN            ((reg32 *) sdlogger_1_irq_write__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the sdlogger_1_irq_write interrupt. */
#define sdlogger_1_irq_write_INTC_CLR_EN            ((reg32 *) sdlogger_1_irq_write__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the sdlogger_1_irq_write interrupt state to pending. */
#define sdlogger_1_irq_write_INTC_SET_PD            ((reg32 *) sdlogger_1_irq_write__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the sdlogger_1_irq_write interrupt. */
#define sdlogger_1_irq_write_INTC_CLR_PD            ((reg32 *) sdlogger_1_irq_write__INTC_CLR_PD_REG)


#endif /* CY_ISR_sdlogger_1_irq_write_H */


/* [] END OF FILE */
