/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"

static char `$INSTANCE_NAME`_fifo[`$INSTANCE_NAME`_FIFO_SIZE];
static int32 `$INSTANCE_NAME`_rptr = 0, `$INSTANCE_NAME`_wptr = 0;

void `$INSTANCE_NAME`_Start(void)
{
	`$INSTANCE_NAME`_Clear(1);
	return;
}

void `$INSTANCE_NAME`_Stop(void)
{
	/* nothing to do */
	return;
}

inline char `$INSTANCE_NAME`_PopChar(void)
{
	register char c;
	c = `$INSTANCE_NAME`_fifo[`$INSTANCE_NAME`_rptr];
	`$INSTANCE_NAME`_rptr = (`$INSTANCE_NAME`_rptr+1) & (`$INSTANCE_NAME`_FIFO_SIZE-1);
	return c;
}

/* 境界をまたぐとうまく動かない実装!!!!! */
inline char* `$INSTANCE_NAME`_PopString(void)
{
	char *ptr = &`$INSTANCE_NAME`_fifo[`$INSTANCE_NAME`_rptr];
	`$INSTANCE_NAME`_PushChar('\0');
	`$INSTANCE_NAME`_rptr = `$INSTANCE_NAME`_wptr;
	return(ptr);
}

inline void `$INSTANCE_NAME`_PushChar(char c)
{
	`$INSTANCE_NAME`_fifo[`$INSTANCE_NAME`_wptr] = c;
	`$INSTANCE_NAME`_wptr = (`$INSTANCE_NAME`_wptr+1) & (`$INSTANCE_NAME`_FIFO_SIZE-1);
	return;
}

inline void `$INSTANCE_NAME`_PushString(char *str)
{
	char c;
	while((c = *str++)) {
		`$INSTANCE_NAME`_PushChar(c);
	}
	return;
}

inline uint16 `$INSTANCE_NAME`_GetContentSize(void)
{
	return((`$INSTANCE_NAME`_wptr - `$INSTANCE_NAME`_rptr) % `$INSTANCE_NAME`_FIFO_SIZE);
}

inline uint16 `$INSTANCE_NAME`_GetVacancySize(void)
{
	int32 offset;
	offset = (`$INSTANCE_NAME`_wptr >= `$INSTANCE_NAME`_rptr) ? `$INSTANCE_NAME`_FIFO_SIZE : 0;
	return(`$INSTANCE_NAME`_rptr + offset - `$INSTANCE_NAME`_wptr);
}

inline void `$INSTANCE_NAME`_Clear(char bufc)
{
	int i;
	`$INSTANCE_NAME`_rptr = 0;
	`$INSTANCE_NAME`_wptr = 0;
	if(bufc) {
		for(i = 0; i < `$INSTANCE_NAME`_FIFO_SIZE; i++) {
			`$INSTANCE_NAME`_fifo[i] = '\0';
		}
	}
	return;
}

/* [] END OF FILE */
