/*******************************************************************************
* File Name: unused2.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_unused2_H) /* Pins unused2_H */
#define CY_PINS_unused2_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "unused2_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_80 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 unused2__PORT == 15 && ((unused2__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    unused2_Write(uint8 value) ;
void    unused2_SetDriveMode(uint8 mode) ;
uint8   unused2_ReadDataReg(void) ;
uint8   unused2_Read(void) ;
uint8   unused2_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define unused2_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define unused2_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define unused2_DM_RES_UP          PIN_DM_RES_UP
#define unused2_DM_RES_DWN         PIN_DM_RES_DWN
#define unused2_DM_OD_LO           PIN_DM_OD_LO
#define unused2_DM_OD_HI           PIN_DM_OD_HI
#define unused2_DM_STRONG          PIN_DM_STRONG
#define unused2_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define unused2_MASK               unused2__MASK
#define unused2_SHIFT              unused2__SHIFT
#define unused2_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define unused2_PS                     (* (reg8 *) unused2__PS)
/* Data Register */
#define unused2_DR                     (* (reg8 *) unused2__DR)
/* Port Number */
#define unused2_PRT_NUM                (* (reg8 *) unused2__PRT) 
/* Connect to Analog Globals */                                                  
#define unused2_AG                     (* (reg8 *) unused2__AG)                       
/* Analog MUX bux enable */
#define unused2_AMUX                   (* (reg8 *) unused2__AMUX) 
/* Bidirectional Enable */                                                        
#define unused2_BIE                    (* (reg8 *) unused2__BIE)
/* Bit-mask for Aliased Register Access */
#define unused2_BIT_MASK               (* (reg8 *) unused2__BIT_MASK)
/* Bypass Enable */
#define unused2_BYP                    (* (reg8 *) unused2__BYP)
/* Port wide control signals */                                                   
#define unused2_CTL                    (* (reg8 *) unused2__CTL)
/* Drive Modes */
#define unused2_DM0                    (* (reg8 *) unused2__DM0) 
#define unused2_DM1                    (* (reg8 *) unused2__DM1)
#define unused2_DM2                    (* (reg8 *) unused2__DM2) 
/* Input Buffer Disable Override */
#define unused2_INP_DIS                (* (reg8 *) unused2__INP_DIS)
/* LCD Common or Segment Drive */
#define unused2_LCD_COM_SEG            (* (reg8 *) unused2__LCD_COM_SEG)
/* Enable Segment LCD */
#define unused2_LCD_EN                 (* (reg8 *) unused2__LCD_EN)
/* Slew Rate Control */
#define unused2_SLW                    (* (reg8 *) unused2__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define unused2_PRTDSI__CAPS_SEL       (* (reg8 *) unused2__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define unused2_PRTDSI__DBL_SYNC_IN    (* (reg8 *) unused2__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define unused2_PRTDSI__OE_SEL0        (* (reg8 *) unused2__PRTDSI__OE_SEL0) 
#define unused2_PRTDSI__OE_SEL1        (* (reg8 *) unused2__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define unused2_PRTDSI__OUT_SEL0       (* (reg8 *) unused2__PRTDSI__OUT_SEL0) 
#define unused2_PRTDSI__OUT_SEL1       (* (reg8 *) unused2__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define unused2_PRTDSI__SYNC_OUT       (* (reg8 *) unused2__PRTDSI__SYNC_OUT) 


#if defined(unused2__INTSTAT)  /* Interrupt Registers */

    #define unused2_INTSTAT                (* (reg8 *) unused2__INTSTAT)
    #define unused2_SNAP                   (* (reg8 *) unused2__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_unused2_H */


/* [] END OF FILE */
