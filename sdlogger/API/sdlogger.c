/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <device.h>
#include <stdlib.h>
#include <stdio.h>

int g_stat;
char g_fname[256];
unsigned char g_wbuf_a[`$INSTANCE_NAME`_WBUF_LENGTH];
unsigned char g_wbuf_b[`$INSTANCE_NAME`_WBUF_LENGTH];
unsigned char *g_wbuf;
int g_wbufpos;
unsigned int g_writtenlen;
FATFS fatfs;
DIR dir;
FIL flog;

void `$INSTANCE_NAME`_WriteBuffer(void);

int `$INSTANCE_NAME`_Start(void)
{	
	sdspi_start();
	g_wbuf = g_wbuf_a;
	g_wbufpos = 0;
	`$INSTANCE_NAME`_irq_write_StartEx(`$INSTANCE_NAME`_WriteBuffer);
	return `$INSTANCE_NAME`_OK;
}

int `$INSTANCE_NAME`_Stop(void)
{
	return `$INSTANCE_NAME`_OK;
}

int `$INSTANCE_NAME`_CreateLog()
{
	int res;
	int i;

//	logger_WaitMilliseconds(100);		// どうする?
//	uflush();
	`$INSTANCE_NAME`_UpdateStatus();
	if(~g_stat & `$INSTANCE_NAME`_LOGGING) {
		`$INSTANCE_NAME`_CloseLog();
	}
	if(~g_stat & `$INSTANCE_NAME`_AVAILABLE) { return `$INSTANCE_NAME`_NODISK; }

	res = f_mount(0, &fatfs);			// Mount drive #0
	if(res != FR_OK) { return `$INSTANCE_NAME`_ERROR; }
	res = f_opendir(&dir, "");			// Open root directory
	if(res != FR_OK) { return `$INSTANCE_NAME`_ERROR; }
	
	for(i = 0; i < 100; i++) {			// check filename from 0
		sprintf(g_fname, `$INSTANCE_NAME`_FILENAME, i);		// TF Log file
		if(!`$INSTANCE_NAME`_Find(g_fname)) { break; }
	}
	flog.fs = &fatfs;
	res = f_open(&flog, g_fname, FA_WRITE | FA_CREATE_ALWAYS);
	if(res == FR_OK) {
		g_stat |= `$INSTANCE_NAME`_LOGGING;
		g_writtenlen = `$INSTANCE_NAME`_WBUF_LENGTH;
		f_sync(&flog);
		return `$INSTANCE_NAME`_OK;
	} else {
		g_stat &= ~`$INSTANCE_NAME`_LOGGING;
		f_close(&flog);
		return `$INSTANCE_NAME`_ERROR;
	}
}

int `$INSTANCE_NAME`_CloseLog(void)
{
	f_close(&flog);
	g_stat &= ~`$INSTANCE_NAME`_LOGGING;
	return `$INSTANCE_NAME`_OK;
}

/* 最低限の書き込み関数 */
int `$INSTANCE_NAME`_Write(char *str, int len)
{
	if(~g_stat & `$INSTANCE_NAME`_LOGGING) {
		return `$INSTANCE_NAME`_ERROR;
	}
	while(len--) {
		g_wbuf[g_wbufpos++] = *str++;
		if(g_wbufpos >= `$INSTANCE_NAME`_WBUF_LENGTH) {
			`$INSTANCE_NAME`_irq_write_SetPending();
			g_wbufpos = 0;
			// swap g_wbuf_a and g_wbuf_b
			if(g_wbuf == g_wbuf_a) {
				g_wbuf = g_wbuf_b;
			} else {
				g_wbuf = g_wbuf_a;
			}
			// 1つ前の書き込みが正常だったかを見る
			if(g_writtenlen != `$INSTANCE_NAME`_WBUF_LENGTH) {
				`$INSTANCE_NAME`_UpdateStatus();
				return `$INSTANCE_NAME`_ERROR;
			}
		}
	}
	return `$INSTANCE_NAME`_OK;
}

int `$INSTANCE_NAME`_WriteLine(char *str, int len)
{
	`$INSTANCE_NAME`_Write(str, len);
	return(`$INSTANCE_NAME`_Write("\r\n", 2));
}

void `$INSTANCE_NAME`_WriteBuffer(void)
{
	f_write(&flog, (g_wbuf == g_wbuf_a) ? g_wbuf_b : g_wbuf_a, `$INSTANCE_NAME`_WBUF_LENGTH, &g_writtenlen);
	f_sync(&flog);
	return;
}

int `$INSTANCE_NAME`_GetStatus(void)
{
	`$INSTANCE_NAME`_UpdateStatus();
	return g_stat;
}

void `$INSTANCE_NAME`_UpdateStatus(void)
{
	if(disk_status(0) & STA_NODISK) {
		g_stat &= ~`$INSTANCE_NAME`_AVAILABLE;
		g_stat &= ~`$INSTANCE_NAME`_LOGGING;
	} else {
		g_stat |= `$INSTANCE_NAME`_AVAILABLE;
	}
	return;
}

char *`$INSTANCE_NAME`_GetFileName(void)
{
	return(g_fname);
}

char `$INSTANCE_NAME`_Find(char *filename)
{
	FILINFO fi;
	return(f_stat(filename, &fi) == FR_OK ? 1 : 0);
}

/* [] END OF FILE */
