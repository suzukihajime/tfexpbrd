/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <device.h>
#include <stdlib.h>
#include <stdio.h>

int g_stat;
char g_fname[256];
unsigned char g_wbuf_a[sdlogger_1_WBUF_LENGTH];
unsigned char g_wbuf_b[sdlogger_1_WBUF_LENGTH];
unsigned char *g_wbuf;
int g_wbufpos;
unsigned int g_writtenlen;
FATFS fatfs;
DIR dir;
FIL flog;

void sdlogger_1_WriteBuffer(void);

int sdlogger_1_Start(void)
{	
	sdspi_start();
	g_wbuf = g_wbuf_a;
	g_wbufpos = 0;
	sdlogger_1_irq_write_StartEx(sdlogger_1_WriteBuffer);
	return sdlogger_1_OK;
}

int sdlogger_1_Stop(void)
{
	return sdlogger_1_OK;
}

int sdlogger_1_CreateLog()
{
	int res;
	int i;

//	logger_WaitMilliseconds(100);		// どうする?
//	uflush();
	sdlogger_1_UpdateStatus();
	if(~g_stat & sdlogger_1_LOGGING) {
		sdlogger_1_CloseLog();
	}
	if(~g_stat & sdlogger_1_AVAILABLE) { return sdlogger_1_NODISK; }

	res = f_mount(0, &fatfs);			// Mount drive #0
	if(res != FR_OK) { return sdlogger_1_ERROR; }
	res = f_opendir(&dir, "");			// Open root directory
	if(res != FR_OK) { return sdlogger_1_ERROR; }
	
	for(i = 0; i < 100; i++) {			// check filename from 0
		sprintf(g_fname, sdlogger_1_FILENAME, i);		// TF Log file
		if(!sdlogger_1_Find(g_fname)) { break; }
	}
	flog.fs = &fatfs;
	res = f_open(&flog, g_fname, FA_WRITE | FA_CREATE_ALWAYS);
	if(res == FR_OK) {
		g_stat |= sdlogger_1_LOGGING;
		g_writtenlen = sdlogger_1_WBUF_LENGTH;
		f_sync(&flog);
		return sdlogger_1_OK;
	} else {
		g_stat &= ~sdlogger_1_LOGGING;
		f_close(&flog);
		return sdlogger_1_ERROR;
	}
}

int sdlogger_1_CloseLog(void)
{
	f_close(&flog);
	g_stat &= ~sdlogger_1_LOGGING;
	return sdlogger_1_OK;
}

/* 最低限の書き込み関数 */
int sdlogger_1_Write(char *str, int len)
{
	if(~g_stat & sdlogger_1_LOGGING) {
		return sdlogger_1_ERROR;
	}
	while(len--) {
		g_wbuf[g_wbufpos++] = *str++;
		if(g_wbufpos >= sdlogger_1_WBUF_LENGTH) {
			sdlogger_1_irq_write_SetPending();
			g_wbufpos = 0;
			// swap g_wbuf_a and g_wbuf_b
			if(g_wbuf == g_wbuf_a) {
				g_wbuf = g_wbuf_b;
			} else {
				g_wbuf = g_wbuf_a;
			}
			// 1つ前の書き込みが正常だったかを見る
			if(g_writtenlen != sdlogger_1_WBUF_LENGTH) {
				sdlogger_1_UpdateStatus();
				return sdlogger_1_ERROR;
			}
		}
	}
	return sdlogger_1_OK;
}

int sdlogger_1_WriteLine(char *str, int len)
{
	sdlogger_1_Write(str, len);
	return(sdlogger_1_Write("\r\n", 2));
}

void sdlogger_1_WriteBuffer(void)
{
	f_write(&flog, (g_wbuf == g_wbuf_a) ? g_wbuf_b : g_wbuf_a, sdlogger_1_WBUF_LENGTH, &g_writtenlen);
	f_sync(&flog);
	return;
}

int sdlogger_1_GetStatus(void)
{
	sdlogger_1_UpdateStatus();
	return g_stat;
}

void sdlogger_1_UpdateStatus(void)
{
	if(disk_status(0) & STA_NODISK) {
		g_stat &= ~sdlogger_1_AVAILABLE;
		g_stat &= ~sdlogger_1_LOGGING;
	} else {
		g_stat |= sdlogger_1_AVAILABLE;
	}
	return;
}

char *sdlogger_1_GetFileName(void)
{
	return(g_fname);
}

char sdlogger_1_Find(char *filename)
{
	FILINFO fi;
	return(f_stat(filename, &fi) == FR_OK ? 1 : 0);
}

/* [] END OF FILE */
