/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <device.h>

char g_tfbuf[256];
int g_tfbufpos;
int g_gpstime;

CY_ISR(onReceive)
{
	char c;
	c = UART_TF_ReadRxData();
	
	if(c == 'N' && g_tfbufpos >= 32) {
		/* Nページの1文字目なので、あまり処理が長いとデータが欠ける */
		sdlogger_1_Write(g_tfbuf, 32);
		/* ここで姿勢データ抽出する? */
		g_gpstime = (int)g_tfbuf[4]
				  | (int)g_tfbuf[5]<<8
				  | (int)g_tfbuf[6]<<16
				  | (int)g_tfbuf[7]<<24;
		/* ここでHページも書いてしまえばよいのではないか */
		
		g_tfbufpos = 0;
	}
	g_tfbuf[g_tfbufpos] = c;
	g_tfbufpos = (g_tfbufpos+1) & 0xff;
	return;
}

void init(void)
{
	buzzer_1_Start();
	buzzer_1_Run(0xc0, buzzer_1_ONESHOT);
	UART_1_Start();
	UART_1_EnableRxInt();
	UART_1_EnableTxInt();
	UART_TF_Start();
	IRQ_TF_StartEx(onReceive);
	g_tfbufpos = 0;
	sdlogger_1_Start();
	btmodule_1_Start();
	return;
}

void wait(void)
{
	int i;
	for(i = 0; i < 10000; i++) {}
	return;
}

void ubputs(char *str)
{
	UART_1_PutString(str);
	if(btmodule_1_GetStatus() & btmodule_1_MODE_BYPASS) {
		btmodule_1_PutString(str);
	}
	return;
}

void main()
{
	char silent = 0;
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	init();
    CyGlobalIntEnable;	 /* Uncomment this line to enable global interrupts. */
	ubputs("Initializing..."); 
	
	while(1) {
        /* Place your application code here. */
		if(~sdlogger_1_GetStatus() & sdlogger_1_LOGGING) {
			if(sdlogger_1_GetStatus() & sdlogger_1_AVAILABLE) {
				if(sdlogger_1_CreateLog() == sdlogger_1_OK) {
					/* silent.txtがあったら音消す */
					if(sdlogger_1_Find("silent.txt")) { silent = 1; }
					/* nobt.txtがあったらBTを動かさない */
					if(sdlogger_1_Find("nobt.txt")) {
						btmodule_1_Stop();
					}
					ubputs("Start logging: ");
					ubputs(sdlogger_1_GetFileName());
					ubputs("\r\n");
					if(!silent) { buzzer_1_Run(0x80, buzzer_1_CONTINUOUS); }
				} else {
					ubputs("ERROR: no response from SD card\r\n");
					if(!silent) { buzzer_1_Run(0xaa, buzzer_1_ONESHOT); }
				}
			} else {
				buzzer_1_Run(0, buzzer_1_ONESHOT);
			}
		}
		if((btmodule_1_GetStatus() & btmodule_1_MODE_BYPASS)
			|| (~btmodule_1_GetStatus() & btmodule_1_RUNNING)) {
			LED_G_Write(0);
		} else {
			LED_G_Write(1);
		}
		wait();
    }
}

DWORD get_fattime(void)
{
	DWORD tmr;
	int h, m, s, ms;
	
	ms = g_gpstime % 1000; g_gpstime /= 1000;
	s = g_gpstime % 60; g_gpstime /= 60;
	m = g_gpstime % 60; g_gpstime /= 60;
	h = g_gpstime % 24; g_gpstime /= 24;
	
	/* Pack date and time into a long variable */
	tmr =	  (((DWORD)2013 - 1980) << 25)
			| ((DWORD)2 << 21)
			| ((DWORD)16 << 16)
			| ((WORD)h << 11)
			| ((WORD)m << 5)
			| ((WORD)s >> 1);
	return tmr;
}

/* [] END OF FILE */
