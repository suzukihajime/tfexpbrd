/*******************************************************************************
* File Name: Pos.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pos_H) /* Pins Pos_H */
#define CY_PINS_Pos_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Pos_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_80 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Pos__PORT == 15 && ((Pos__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    Pos_Write(uint8 value) ;
void    Pos_SetDriveMode(uint8 mode) ;
uint8   Pos_ReadDataReg(void) ;
uint8   Pos_Read(void) ;
uint8   Pos_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Pos_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Pos_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Pos_DM_RES_UP          PIN_DM_RES_UP
#define Pos_DM_RES_DWN         PIN_DM_RES_DWN
#define Pos_DM_OD_LO           PIN_DM_OD_LO
#define Pos_DM_OD_HI           PIN_DM_OD_HI
#define Pos_DM_STRONG          PIN_DM_STRONG
#define Pos_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Pos_MASK               Pos__MASK
#define Pos_SHIFT              Pos__SHIFT
#define Pos_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Pos_PS                     (* (reg8 *) Pos__PS)
/* Data Register */
#define Pos_DR                     (* (reg8 *) Pos__DR)
/* Port Number */
#define Pos_PRT_NUM                (* (reg8 *) Pos__PRT) 
/* Connect to Analog Globals */                                                  
#define Pos_AG                     (* (reg8 *) Pos__AG)                       
/* Analog MUX bux enable */
#define Pos_AMUX                   (* (reg8 *) Pos__AMUX) 
/* Bidirectional Enable */                                                        
#define Pos_BIE                    (* (reg8 *) Pos__BIE)
/* Bit-mask for Aliased Register Access */
#define Pos_BIT_MASK               (* (reg8 *) Pos__BIT_MASK)
/* Bypass Enable */
#define Pos_BYP                    (* (reg8 *) Pos__BYP)
/* Port wide control signals */                                                   
#define Pos_CTL                    (* (reg8 *) Pos__CTL)
/* Drive Modes */
#define Pos_DM0                    (* (reg8 *) Pos__DM0) 
#define Pos_DM1                    (* (reg8 *) Pos__DM1)
#define Pos_DM2                    (* (reg8 *) Pos__DM2) 
/* Input Buffer Disable Override */
#define Pos_INP_DIS                (* (reg8 *) Pos__INP_DIS)
/* LCD Common or Segment Drive */
#define Pos_LCD_COM_SEG            (* (reg8 *) Pos__LCD_COM_SEG)
/* Enable Segment LCD */
#define Pos_LCD_EN                 (* (reg8 *) Pos__LCD_EN)
/* Slew Rate Control */
#define Pos_SLW                    (* (reg8 *) Pos__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Pos_PRTDSI__CAPS_SEL       (* (reg8 *) Pos__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Pos_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Pos__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Pos_PRTDSI__OE_SEL0        (* (reg8 *) Pos__PRTDSI__OE_SEL0) 
#define Pos_PRTDSI__OE_SEL1        (* (reg8 *) Pos__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Pos_PRTDSI__OUT_SEL0       (* (reg8 *) Pos__PRTDSI__OUT_SEL0) 
#define Pos_PRTDSI__OUT_SEL1       (* (reg8 *) Pos__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Pos_PRTDSI__SYNC_OUT       (* (reg8 *) Pos__PRTDSI__SYNC_OUT) 


#if defined(Pos__INTSTAT)  /* Interrupt Registers */

    #define Pos_INTSTAT                (* (reg8 *) Pos__INTSTAT)
    #define Pos_SNAP                   (* (reg8 *) Pos__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Pos_H */


/* [] END OF FILE */
