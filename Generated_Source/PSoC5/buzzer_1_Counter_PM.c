/*******************************************************************************
* File Name: buzzer_1_Counter_PM.c  
* Version 2.30
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "buzzer_1_Counter.h"

static buzzer_1_Counter_backupStruct buzzer_1_Counter_backup;


/*******************************************************************************
* Function Name: buzzer_1_Counter_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  buzzer_1_Counter_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void buzzer_1_Counter_SaveConfig(void) 
{
    #if (!buzzer_1_Counter_UsingFixedFunction)
        /* Backup the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            buzzer_1_Counter_backup.CounterUdb = buzzer_1_Counter_ReadCounter();
            buzzer_1_Counter_backup.CounterPeriod = buzzer_1_Counter_ReadPeriod();
            buzzer_1_Counter_backup.CompareValue = buzzer_1_Counter_ReadCompare();
            buzzer_1_Counter_backup.InterruptMaskValue = buzzer_1_Counter_STATUS_MASK;
        #endif /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            buzzer_1_Counter_backup.CounterUdb = buzzer_1_Counter_ReadCounter();
            buzzer_1_Counter_backup.InterruptMaskValue = buzzer_1_Counter_STATUS_MASK;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!buzzer_1_Counter_ControlRegRemoved)
            buzzer_1_Counter_backup.CounterControlRegister = buzzer_1_Counter_ReadControlRegister();
        #endif /* (!buzzer_1_Counter_ControlRegRemoved) */
    #endif /* (!buzzer_1_Counter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: buzzer_1_Counter_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  buzzer_1_Counter_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void buzzer_1_Counter_RestoreConfig(void) 
{      
    #if (!buzzer_1_Counter_UsingFixedFunction)     
        /* Restore the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            /* Interrupt State Backup for Critical Region*/
            uint8 buzzer_1_Counter_interruptState;
        
            buzzer_1_Counter_WriteCounter(buzzer_1_Counter_backup.CounterUdb);
            buzzer_1_Counter_WritePeriod(buzzer_1_Counter_backup.CounterPeriod);
            buzzer_1_Counter_WriteCompare(buzzer_1_Counter_backup.CompareValue);
            /* Enter Critical Region*/
            buzzer_1_Counter_interruptState = CyEnterCriticalSection();
        
            buzzer_1_Counter_STATUS_AUX_CTRL |= buzzer_1_Counter_STATUS_ACTL_INT_EN_MASK;
            /* Exit Critical Region*/
            CyExitCriticalSection(buzzer_1_Counter_interruptState);
            buzzer_1_Counter_STATUS_MASK = buzzer_1_Counter_backup.InterruptMaskValue;
        #endif  /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            buzzer_1_Counter_WriteCounter(buzzer_1_Counter_backup.CounterUdb);
            buzzer_1_Counter_STATUS_MASK = buzzer_1_Counter_backup.InterruptMaskValue;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!buzzer_1_Counter_ControlRegRemoved)
            buzzer_1_Counter_WriteControlRegister(buzzer_1_Counter_backup.CounterControlRegister);
        #endif /* (!buzzer_1_Counter_ControlRegRemoved) */
    #endif /* (!buzzer_1_Counter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: buzzer_1_Counter_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  buzzer_1_Counter_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void buzzer_1_Counter_Sleep(void) 
{
    #if(!buzzer_1_Counter_ControlRegRemoved)
        /* Save Counter's enable state */
        if(buzzer_1_Counter_CTRL_ENABLE == (buzzer_1_Counter_CONTROL & buzzer_1_Counter_CTRL_ENABLE))
        {
            /* Counter is enabled */
            buzzer_1_Counter_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            buzzer_1_Counter_backup.CounterEnableState = 0u;
        }
    #else
        buzzer_1_Counter_backup.CounterEnableState = 1u;
        if(buzzer_1_Counter_backup.CounterEnableState != 0u)
        {
            buzzer_1_Counter_backup.CounterEnableState = 0u;
        }
    #endif /* (!buzzer_1_Counter_ControlRegRemoved) */
    
    buzzer_1_Counter_Stop();
    buzzer_1_Counter_SaveConfig();
}


/*******************************************************************************
* Function Name: buzzer_1_Counter_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  buzzer_1_Counter_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void buzzer_1_Counter_Wakeup(void) 
{
    buzzer_1_Counter_RestoreConfig();
    #if(!buzzer_1_Counter_ControlRegRemoved)
        if(buzzer_1_Counter_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            buzzer_1_Counter_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!buzzer_1_Counter_ControlRegRemoved) */
    
}


/* [] END OF FILE */
