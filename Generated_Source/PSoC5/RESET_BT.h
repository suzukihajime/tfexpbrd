/*******************************************************************************
* File Name: RESET_BT.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_RESET_BT_H) /* Pins RESET_BT_H */
#define CY_PINS_RESET_BT_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "RESET_BT_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_80 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 RESET_BT__PORT == 15 && ((RESET_BT__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    RESET_BT_Write(uint8 value) ;
void    RESET_BT_SetDriveMode(uint8 mode) ;
uint8   RESET_BT_ReadDataReg(void) ;
uint8   RESET_BT_Read(void) ;
uint8   RESET_BT_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define RESET_BT_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define RESET_BT_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define RESET_BT_DM_RES_UP          PIN_DM_RES_UP
#define RESET_BT_DM_RES_DWN         PIN_DM_RES_DWN
#define RESET_BT_DM_OD_LO           PIN_DM_OD_LO
#define RESET_BT_DM_OD_HI           PIN_DM_OD_HI
#define RESET_BT_DM_STRONG          PIN_DM_STRONG
#define RESET_BT_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define RESET_BT_MASK               RESET_BT__MASK
#define RESET_BT_SHIFT              RESET_BT__SHIFT
#define RESET_BT_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define RESET_BT_PS                     (* (reg8 *) RESET_BT__PS)
/* Data Register */
#define RESET_BT_DR                     (* (reg8 *) RESET_BT__DR)
/* Port Number */
#define RESET_BT_PRT_NUM                (* (reg8 *) RESET_BT__PRT) 
/* Connect to Analog Globals */                                                  
#define RESET_BT_AG                     (* (reg8 *) RESET_BT__AG)                       
/* Analog MUX bux enable */
#define RESET_BT_AMUX                   (* (reg8 *) RESET_BT__AMUX) 
/* Bidirectional Enable */                                                        
#define RESET_BT_BIE                    (* (reg8 *) RESET_BT__BIE)
/* Bit-mask for Aliased Register Access */
#define RESET_BT_BIT_MASK               (* (reg8 *) RESET_BT__BIT_MASK)
/* Bypass Enable */
#define RESET_BT_BYP                    (* (reg8 *) RESET_BT__BYP)
/* Port wide control signals */                                                   
#define RESET_BT_CTL                    (* (reg8 *) RESET_BT__CTL)
/* Drive Modes */
#define RESET_BT_DM0                    (* (reg8 *) RESET_BT__DM0) 
#define RESET_BT_DM1                    (* (reg8 *) RESET_BT__DM1)
#define RESET_BT_DM2                    (* (reg8 *) RESET_BT__DM2) 
/* Input Buffer Disable Override */
#define RESET_BT_INP_DIS                (* (reg8 *) RESET_BT__INP_DIS)
/* LCD Common or Segment Drive */
#define RESET_BT_LCD_COM_SEG            (* (reg8 *) RESET_BT__LCD_COM_SEG)
/* Enable Segment LCD */
#define RESET_BT_LCD_EN                 (* (reg8 *) RESET_BT__LCD_EN)
/* Slew Rate Control */
#define RESET_BT_SLW                    (* (reg8 *) RESET_BT__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define RESET_BT_PRTDSI__CAPS_SEL       (* (reg8 *) RESET_BT__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define RESET_BT_PRTDSI__DBL_SYNC_IN    (* (reg8 *) RESET_BT__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define RESET_BT_PRTDSI__OE_SEL0        (* (reg8 *) RESET_BT__PRTDSI__OE_SEL0) 
#define RESET_BT_PRTDSI__OE_SEL1        (* (reg8 *) RESET_BT__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define RESET_BT_PRTDSI__OUT_SEL0       (* (reg8 *) RESET_BT__PRTDSI__OUT_SEL0) 
#define RESET_BT_PRTDSI__OUT_SEL1       (* (reg8 *) RESET_BT__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define RESET_BT_PRTDSI__SYNC_OUT       (* (reg8 *) RESET_BT__PRTDSI__SYNC_OUT) 


#if defined(RESET_BT__INTSTAT)  /* Interrupt Registers */

    #define RESET_BT_INTSTAT                (* (reg8 *) RESET_BT__INTSTAT)
    #define RESET_BT_SNAP                   (* (reg8 *) RESET_BT__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_RESET_BT_H */


/* [] END OF FILE */
