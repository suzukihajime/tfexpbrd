/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"
#include <string.h>

char btmodule_1_stat;

CY_ISR(btmodule_1_RX_ISR)
{
	char c;
	c = btmodule_1_UART_ReadRxData();
	if(btmodule_1_stat & btmodule_1_MODE_COMM) {
		/* コマンドモードだったら */
		if(c == '\r') {
			if(strcmp(btmodule_1_rxfifo_PopString(), btmodule_1_STR_BYPASS_MODE) == 0) {
				btmodule_1_stat &= ~btmodule_1_MODE_COMM;
				btmodule_1_stat |= btmodule_1_MODE_BYPASS;
				UART_1_PutChar('!');
			}
			btmodule_1_rxfifo_Clear(0);
		} else if(c != '\n') {
			btmodule_1_rxfifo_PushChar(c);
		}
	} else {
		btmodule_1_rxfifo_PushChar(c);
	}
	return;
}

CY_ISR(btmodule_1_TX_ISR)
{
	if(btmodule_1_txfifo_GetContentSize()) {
		btmodule_1_UART_PutChar(btmodule_1_txfifo_PopChar());
	}
	return;
}

void btmodule_1_Start(void)
{
	int i;
	/* 変数初期化 */
	btmodule_1_stat = btmodule_1_MODE_COMM;
	/* 割り込み初期化 */
	btmodule_1_IRQ_RX_StartEx(btmodule_1_RX_ISR);
	btmodule_1_IRQ_TX_StartEx(btmodule_1_TX_ISR);
	/* UART初期化 */
	btmodule_1_UART_Start();
	/* fifo初期化 */
	btmodule_1_rxfifo_Start();
	btmodule_1_txfifo_Start();
	/* BT moduleリセット */
	btmodule_1_Control_Reg_Write(0);
	for(i = 0; i < 10000; i++) {}
	btmodule_1_Control_Reg_Write(1);
	btmodule_1_stat |= btmodule_1_RUNNING;
	return;
}

void btmodule_1_Stop(void)
{
	btmodule_1_UART_Stop();
	btmodule_1_Control_Reg_Write(0);
	btmodule_1_stat &= ~btmodule_1_RUNNING;
	return;
}

char btmodule_1_GetChar(void)
{
	return(btmodule_1_rxfifo_PopChar());
}

char *btmodule_1_GetString(void)
{
	return(btmodule_1_rxfifo_PopString());
}

void btmodule_1_PutChar(char c)
{
	btmodule_1_UART_PutChar(c);
	return;
}

void btmodule_1_PutString(char *str)
{
	while(*str != '\0') {
		btmodule_1_txfifo_PushChar(*str++);
	}
	if(btmodule_1_txfifo_GetContentSize()) {
		btmodule_1_UART_PutChar(btmodule_1_txfifo_PopChar());
	}
	return;
}

char btmodule_1_GetStatus(void)
{
	if(btmodule_1_txfifo_GetContentSize()) {
		btmodule_1_stat |= btmodule_1_TXFIFO_NEMPTY;
	} else {
		btmodule_1_stat &= ~btmodule_1_TXFIFO_NEMPTY;
	}
	if(btmodule_1_rxfifo_GetContentSize()) {
		btmodule_1_stat |= btmodule_1_RXFIFO_NEMPTY;
	} else {
		btmodule_1_stat &= ~btmodule_1_RXFIFO_NEMPTY;
	}
	return(btmodule_1_stat);
}

/* [] END OF FILE */
