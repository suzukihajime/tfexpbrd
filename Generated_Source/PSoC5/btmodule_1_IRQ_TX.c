/*******************************************************************************
* File Name: btmodule_1_IRQ_TX.c  
* Version 1.70
*
*  Description:
*   API for controlling the state of an interrupt.
*
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/


#include <CYDEVICE_TRM.H>
#include <CYLIB.H>
#include <btmodule_1_IRQ_TX.H>

#if !defined(btmodule_1_IRQ_TX__REMOVED) /* Check for removal by optimization */

/*******************************************************************************
*  Place your includes, defines and code here 
********************************************************************************/
/* `#START btmodule_1_IRQ_TX_intc` */

/* `#END` */

#ifndef CYINT_IRQ_BASE
#define CYINT_IRQ_BASE      16
#endif /* CYINT_IRQ_BASE */
#ifndef CYINT_VECT_TABLE
#define CYINT_VECT_TABLE    ((cyisraddress **) CYREG_NVIC_VECT_OFFSET)
#endif /* CYINT_VECT_TABLE */

/* Declared in startup, used to set unused interrupts to. */
CY_ISR_PROTO(IntDefaultHandler);


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_Start
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   None
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_Start(void)
{
    /* For all we know the interrupt is active. */
    btmodule_1_IRQ_TX_Disable();

    /* Set the ISR to point to the btmodule_1_IRQ_TX Interrupt. */
    btmodule_1_IRQ_TX_SetVector(&btmodule_1_IRQ_TX_Interrupt);

    /* Set the priority. */
    btmodule_1_IRQ_TX_SetPriority((uint8)btmodule_1_IRQ_TX_INTC_PRIOR_NUMBER);

    /* Enable it. */
    btmodule_1_IRQ_TX_Enable();
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_StartEx
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_StartEx(cyisraddress address)
{
    /* For all we know the interrupt is active. */
    btmodule_1_IRQ_TX_Disable();

    /* Set the ISR to point to the btmodule_1_IRQ_TX Interrupt. */
    btmodule_1_IRQ_TX_SetVector(address);

    /* Set the priority. */
    btmodule_1_IRQ_TX_SetPriority((uint8)btmodule_1_IRQ_TX_INTC_PRIOR_NUMBER);

    /* Enable it. */
    btmodule_1_IRQ_TX_Enable();
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_Stop
********************************************************************************
*
* Summary:
*   Disables and removes the interrupt.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_Stop(void)
{
    /* Disable this interrupt. */
    btmodule_1_IRQ_TX_Disable();

    /* Set the ISR to point to the passive one. */
    btmodule_1_IRQ_TX_SetVector(&IntDefaultHandler);
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_Interrupt
********************************************************************************
*
* Summary:
*   The default Interrupt Service Routine for btmodule_1_IRQ_TX.
*
*   Add custom code between the coments to keep the next version of this file
*   from over writting your code.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
CY_ISR(btmodule_1_IRQ_TX_Interrupt)
{
    /*  Place your Interrupt code here. */
    /* `#START btmodule_1_IRQ_TX_Interrupt` */

    /* `#END` */
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_SetVector
********************************************************************************
*
* Summary:
*   Change the ISR vector for the Interrupt. Note calling btmodule_1_IRQ_TX_Start
*   will override any effect this method would have had. To set the vector 
*   before the component has been started use btmodule_1_IRQ_TX_StartEx instead.
*
* Parameters:
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_SetVector(cyisraddress address)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    ramVectorTable[CYINT_IRQ_BASE + (uint32)btmodule_1_IRQ_TX__INTC_NUMBER] = address;
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_GetVector
********************************************************************************
*
* Summary:
*   Gets the "address" of the current ISR vector for the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Address of the ISR in the interrupt vector table.
*
*******************************************************************************/
cyisraddress btmodule_1_IRQ_TX_GetVector(void)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    return ramVectorTable[CYINT_IRQ_BASE + (uint32)btmodule_1_IRQ_TX__INTC_NUMBER];
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_SetPriority
********************************************************************************
*
* Summary:
*   Sets the Priority of the Interrupt. Note calling btmodule_1_IRQ_TX_Start
*   or btmodule_1_IRQ_TX_StartEx will override any effect this method 
*   would have had. This method should only be called after 
*   btmodule_1_IRQ_TX_Start or btmodule_1_IRQ_TX_StartEx has been called. To set 
*   the initial priority for the component use the cydwr file in the tool.
*
* Parameters:
*   priority: Priority of the interrupt. 0 - 7, 0 being the highest.
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_SetPriority(uint8 priority)
{
    *btmodule_1_IRQ_TX_INTC_PRIOR = priority << 5;
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_GetPriority
********************************************************************************
*
* Summary:
*   Gets the Priority of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Priority of the interrupt. 0 - 7, 0 being the highest.
*
*******************************************************************************/
uint8 btmodule_1_IRQ_TX_GetPriority(void)
{
    uint8 priority;


    priority = *btmodule_1_IRQ_TX_INTC_PRIOR >> 5;

    return priority;
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_Enable
********************************************************************************
*
* Summary:
*   Enables the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_Enable(void)
{
    /* Enable the general interrupt. */
    *btmodule_1_IRQ_TX_INTC_SET_EN = btmodule_1_IRQ_TX__INTC_MASK;
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_GetState
********************************************************************************
*
* Summary:
*   Gets the state (enabled, disabled) of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   1 if enabled, 0 if disabled.
*
*******************************************************************************/
uint8 btmodule_1_IRQ_TX_GetState(void)
{
    /* Get the state of the general interrupt. */
    return ((*btmodule_1_IRQ_TX_INTC_SET_EN & (uint32)btmodule_1_IRQ_TX__INTC_MASK) != 0u) ? 1u:0u;
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_Disable
********************************************************************************
*
* Summary:
*   Disables the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_Disable(void)
{
    /* Disable the general interrupt. */
    *btmodule_1_IRQ_TX_INTC_CLR_EN = btmodule_1_IRQ_TX__INTC_MASK;
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_SetPending
********************************************************************************
*
* Summary:
*   Causes the Interrupt to enter the pending state, a software method of
*   generating the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_SetPending(void)
{
    *btmodule_1_IRQ_TX_INTC_SET_PD = btmodule_1_IRQ_TX__INTC_MASK;
}


/*******************************************************************************
* Function Name: btmodule_1_IRQ_TX_ClearPending
********************************************************************************
*
* Summary:
*   Clears a pending interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void btmodule_1_IRQ_TX_ClearPending(void)
{
    *btmodule_1_IRQ_TX_INTC_CLR_PD = btmodule_1_IRQ_TX__INTC_MASK;
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
