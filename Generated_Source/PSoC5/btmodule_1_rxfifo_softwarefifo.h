/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

#define	btmodule_1_rxfifo_FIFO_SIZE		(1024)

void btmodule_1_rxfifo_Start(void);
void btmodule_1_rxfifo_Stop(void);
inline char btmodule_1_rxfifo_PopChar(void);
inline char* btmodule_1_rxfifo_PopString(void);
inline void btmodule_1_rxfifo_PushChar(char c);
inline void btmodule_1_rxfifo_PushString(char *str);
inline uint16 btmodule_1_rxfifo_GetContentSize(void);
inline uint16 btmodule_1_rxfifo_GetVacancySize(void);
inline void btmodule_1_rxfifo_Clear(char bufc);
//[] END OF FILE
