/*******************************************************************************
* File Name: Pos.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pos_ALIASES_H) /* Pins Pos_ALIASES_H */
#define CY_PINS_Pos_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"



/***************************************
*              Constants        
***************************************/
#define Pos_0		Pos__0__PC

#endif /* End Pins Pos_ALIASES_H */

/* [] END OF FILE */
