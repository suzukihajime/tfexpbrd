/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

#if !defined(sdlogger_1_sdspi_1_H)
#define	sdlogger_1_sdspi_1_H

void sdspi_start(void);
void sdspi_stop(void);

#include "sdlogger_1_sdspi_1_diskio.h"

#endif	/* end of #if !defined(sdlogger_1_sdspi_1_H) */

//[] END OF FILE
