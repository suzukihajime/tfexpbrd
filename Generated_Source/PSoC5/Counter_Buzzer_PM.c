/*******************************************************************************
* File Name: Counter_Buzzer_PM.c  
* Version 2.30
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "Counter_Buzzer.h"

static Counter_Buzzer_backupStruct Counter_Buzzer_backup;


/*******************************************************************************
* Function Name: Counter_Buzzer_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Counter_Buzzer_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void Counter_Buzzer_SaveConfig(void) 
{
    #if (!Counter_Buzzer_UsingFixedFunction)
        /* Backup the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            Counter_Buzzer_backup.CounterUdb = Counter_Buzzer_ReadCounter();
            Counter_Buzzer_backup.CounterPeriod = Counter_Buzzer_ReadPeriod();
            Counter_Buzzer_backup.CompareValue = Counter_Buzzer_ReadCompare();
            Counter_Buzzer_backup.InterruptMaskValue = Counter_Buzzer_STATUS_MASK;
        #endif /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            Counter_Buzzer_backup.CounterUdb = Counter_Buzzer_ReadCounter();
            Counter_Buzzer_backup.InterruptMaskValue = Counter_Buzzer_STATUS_MASK;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!Counter_Buzzer_ControlRegRemoved)
            Counter_Buzzer_backup.CounterControlRegister = Counter_Buzzer_ReadControlRegister();
        #endif /* (!Counter_Buzzer_ControlRegRemoved) */
    #endif /* (!Counter_Buzzer_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Counter_Buzzer_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Counter_Buzzer_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Counter_Buzzer_RestoreConfig(void) 
{      
    #if (!Counter_Buzzer_UsingFixedFunction)     
        /* Restore the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            /* Interrupt State Backup for Critical Region*/
            uint8 Counter_Buzzer_interruptState;
        
            Counter_Buzzer_WriteCounter(Counter_Buzzer_backup.CounterUdb);
            Counter_Buzzer_WritePeriod(Counter_Buzzer_backup.CounterPeriod);
            Counter_Buzzer_WriteCompare(Counter_Buzzer_backup.CompareValue);
            /* Enter Critical Region*/
            Counter_Buzzer_interruptState = CyEnterCriticalSection();
        
            Counter_Buzzer_STATUS_AUX_CTRL |= Counter_Buzzer_STATUS_ACTL_INT_EN_MASK;
            /* Exit Critical Region*/
            CyExitCriticalSection(Counter_Buzzer_interruptState);
            Counter_Buzzer_STATUS_MASK = Counter_Buzzer_backup.InterruptMaskValue;
        #endif  /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            Counter_Buzzer_WriteCounter(Counter_Buzzer_backup.CounterUdb);
            Counter_Buzzer_STATUS_MASK = Counter_Buzzer_backup.InterruptMaskValue;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!Counter_Buzzer_ControlRegRemoved)
            Counter_Buzzer_WriteControlRegister(Counter_Buzzer_backup.CounterControlRegister);
        #endif /* (!Counter_Buzzer_ControlRegRemoved) */
    #endif /* (!Counter_Buzzer_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Counter_Buzzer_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Counter_Buzzer_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void Counter_Buzzer_Sleep(void) 
{
    #if(!Counter_Buzzer_ControlRegRemoved)
        /* Save Counter's enable state */
        if(Counter_Buzzer_CTRL_ENABLE == (Counter_Buzzer_CONTROL & Counter_Buzzer_CTRL_ENABLE))
        {
            /* Counter is enabled */
            Counter_Buzzer_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            Counter_Buzzer_backup.CounterEnableState = 0u;
        }
    #else
        Counter_Buzzer_backup.CounterEnableState = 1u;
        if(Counter_Buzzer_backup.CounterEnableState != 0u)
        {
            Counter_Buzzer_backup.CounterEnableState = 0u;
        }
    #endif /* (!Counter_Buzzer_ControlRegRemoved) */
    
    Counter_Buzzer_Stop();
    Counter_Buzzer_SaveConfig();
}


/*******************************************************************************
* Function Name: Counter_Buzzer_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Counter_Buzzer_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Counter_Buzzer_Wakeup(void) 
{
    Counter_Buzzer_RestoreConfig();
    #if(!Counter_Buzzer_ControlRegRemoved)
        if(Counter_Buzzer_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            Counter_Buzzer_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!Counter_Buzzer_ControlRegRemoved) */
    
}


/* [] END OF FILE */
