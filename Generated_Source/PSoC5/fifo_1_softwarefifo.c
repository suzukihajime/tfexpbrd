/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"

static char fifo_1_fifo[fifo_1_FIFO_SIZE];
static int32 fifo_1_rptr = 0, fifo_1_wptr = 1;

void fifo_1_Start(void)
{
	fifo_1_Clear();
	return;
}

void fifo_1_Stop(void)
{
	/* nothing to do */
	return;
}


inline char fifo_1_PopChar(void)
{
	register char c;
	c = fifo_1_fifo[fifo_1_rptr];
	fifo_1_rptr = (fifo_1_rptr+1) % fifo_1_FIFO_SIZE;
	return c;
}

inline void fifo_1_PushChar(char c)
{
	fifo_1_fifo[fifo_1_wptr] = c;
	fifo_1_wptr = (fifo_1_wptr+1) % fifo_1_FIFO_SIZE;
	return;
}

inline void fifo_1_PushString(char *str)
{
	char c;
	while((c = *str++)) {
		fifo_1_PushChar(c);
	}
	return;
}

inline uint16 fifo_1_GetContentSize(void)
{
	return((fifo_1_wptr - fifo_1_rptr) % fifo_1_FIFO_SIZE);
}

inline uint16 fifo_1_GetVacancySize(void)
{
	int32 offset;
	offset = (fifo_1_wptr >= fifo_1_rptr) ? fifo_1_FIFO_SIZE : 0;
	return(fifo_1_rptr + offset - fifo_1_wptr);
}

inline void fifo_1_Clear(void)
{
	int i;
	fifo_1_rptr = 0;
	fifo_1_wptr = 1;
	for(i = 0; i < fifo_1_FIFO_SIZE; i++) {
		fifo_1_fifo[i] = '\0';
	}
	return;
}

/* [] END OF FILE */
