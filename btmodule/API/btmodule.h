/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

#define	`$INSTANCE_NAME`_STR_BYPASS_MODE	"AT-AB -BypassMode-"
#define	`$INSTANCE_NAME`_STR_COMMAND_MODE	"AT-AB -CommandMode-"

#define	`$INSTANCE_NAME`_RUNNING			(1)
#define	`$INSTANCE_NAME`_MODE_COMM			(2)
#define	`$INSTANCE_NAME`_MODE_BYPASS		(4)
#define	`$INSTANCE_NAME`_TXFIFO_NEMPTY		(8)
#define	`$INSTANCE_NAME`_RXFIFO_NEMPTY		(16)

void `$INSTANCE_NAME`_RX_ISR(void);
void `$INSTANCE_NAME`_TX_ISR(void);
void `$INSTANCE_NAME`_Start(void);
void `$INSTANCE_NAME`_Stop(void);
void `$INSTANCE_NAME`_PutChar(char);
void `$INSTANCE_NAME`_PutString(char*);
char `$INSTANCE_NAME`_GetChar(void);
char *`$INSTANCE_NAME`_GetString(void);
char `$INSTANCE_NAME`_GetStatus(void);

//[] END OF FILE
