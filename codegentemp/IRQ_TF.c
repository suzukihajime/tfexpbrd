/*******************************************************************************
* File Name: IRQ_TF.c  
* Version 1.70
*
*  Description:
*   API for controlling the state of an interrupt.
*
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/


#include <CYDEVICE_TRM.H>
#include <CYLIB.H>
#include <IRQ_TF.H>

#if !defined(IRQ_TF__REMOVED) /* Check for removal by optimization */

/*******************************************************************************
*  Place your includes, defines and code here 
********************************************************************************/
/* `#START IRQ_TF_intc` */

/* `#END` */

#ifndef CYINT_IRQ_BASE
#define CYINT_IRQ_BASE      16
#endif /* CYINT_IRQ_BASE */
#ifndef CYINT_VECT_TABLE
#define CYINT_VECT_TABLE    ((cyisraddress **) CYREG_NVIC_VECT_OFFSET)
#endif /* CYINT_VECT_TABLE */

/* Declared in startup, used to set unused interrupts to. */
CY_ISR_PROTO(IntDefaultHandler);


/*******************************************************************************
* Function Name: IRQ_TF_Start
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   None
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_Start(void)
{
    /* For all we know the interrupt is active. */
    IRQ_TF_Disable();

    /* Set the ISR to point to the IRQ_TF Interrupt. */
    IRQ_TF_SetVector(&IRQ_TF_Interrupt);

    /* Set the priority. */
    IRQ_TF_SetPriority((uint8)IRQ_TF_INTC_PRIOR_NUMBER);

    /* Enable it. */
    IRQ_TF_Enable();
}


/*******************************************************************************
* Function Name: IRQ_TF_StartEx
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_StartEx(cyisraddress address)
{
    /* For all we know the interrupt is active. */
    IRQ_TF_Disable();

    /* Set the ISR to point to the IRQ_TF Interrupt. */
    IRQ_TF_SetVector(address);

    /* Set the priority. */
    IRQ_TF_SetPriority((uint8)IRQ_TF_INTC_PRIOR_NUMBER);

    /* Enable it. */
    IRQ_TF_Enable();
}


/*******************************************************************************
* Function Name: IRQ_TF_Stop
********************************************************************************
*
* Summary:
*   Disables and removes the interrupt.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_Stop(void)
{
    /* Disable this interrupt. */
    IRQ_TF_Disable();

    /* Set the ISR to point to the passive one. */
    IRQ_TF_SetVector(&IntDefaultHandler);
}


/*******************************************************************************
* Function Name: IRQ_TF_Interrupt
********************************************************************************
*
* Summary:
*   The default Interrupt Service Routine for IRQ_TF.
*
*   Add custom code between the coments to keep the next version of this file
*   from over writting your code.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
CY_ISR(IRQ_TF_Interrupt)
{
    /*  Place your Interrupt code here. */
    /* `#START IRQ_TF_Interrupt` */

    /* `#END` */
}


/*******************************************************************************
* Function Name: IRQ_TF_SetVector
********************************************************************************
*
* Summary:
*   Change the ISR vector for the Interrupt. Note calling IRQ_TF_Start
*   will override any effect this method would have had. To set the vector 
*   before the component has been started use IRQ_TF_StartEx instead.
*
* Parameters:
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_SetVector(cyisraddress address)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    ramVectorTable[CYINT_IRQ_BASE + (uint32)IRQ_TF__INTC_NUMBER] = address;
}


/*******************************************************************************
* Function Name: IRQ_TF_GetVector
********************************************************************************
*
* Summary:
*   Gets the "address" of the current ISR vector for the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Address of the ISR in the interrupt vector table.
*
*******************************************************************************/
cyisraddress IRQ_TF_GetVector(void)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    return ramVectorTable[CYINT_IRQ_BASE + (uint32)IRQ_TF__INTC_NUMBER];
}


/*******************************************************************************
* Function Name: IRQ_TF_SetPriority
********************************************************************************
*
* Summary:
*   Sets the Priority of the Interrupt. Note calling IRQ_TF_Start
*   or IRQ_TF_StartEx will override any effect this method 
*   would have had. This method should only be called after 
*   IRQ_TF_Start or IRQ_TF_StartEx has been called. To set 
*   the initial priority for the component use the cydwr file in the tool.
*
* Parameters:
*   priority: Priority of the interrupt. 0 - 7, 0 being the highest.
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_SetPriority(uint8 priority)
{
    *IRQ_TF_INTC_PRIOR = priority << 5;
}


/*******************************************************************************
* Function Name: IRQ_TF_GetPriority
********************************************************************************
*
* Summary:
*   Gets the Priority of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Priority of the interrupt. 0 - 7, 0 being the highest.
*
*******************************************************************************/
uint8 IRQ_TF_GetPriority(void)
{
    uint8 priority;


    priority = *IRQ_TF_INTC_PRIOR >> 5;

    return priority;
}


/*******************************************************************************
* Function Name: IRQ_TF_Enable
********************************************************************************
*
* Summary:
*   Enables the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_Enable(void)
{
    /* Enable the general interrupt. */
    *IRQ_TF_INTC_SET_EN = IRQ_TF__INTC_MASK;
}


/*******************************************************************************
* Function Name: IRQ_TF_GetState
********************************************************************************
*
* Summary:
*   Gets the state (enabled, disabled) of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   1 if enabled, 0 if disabled.
*
*******************************************************************************/
uint8 IRQ_TF_GetState(void)
{
    /* Get the state of the general interrupt. */
    return ((*IRQ_TF_INTC_SET_EN & (uint32)IRQ_TF__INTC_MASK) != 0u) ? 1u:0u;
}


/*******************************************************************************
* Function Name: IRQ_TF_Disable
********************************************************************************
*
* Summary:
*   Disables the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_Disable(void)
{
    /* Disable the general interrupt. */
    *IRQ_TF_INTC_CLR_EN = IRQ_TF__INTC_MASK;
}


/*******************************************************************************
* Function Name: IRQ_TF_SetPending
********************************************************************************
*
* Summary:
*   Causes the Interrupt to enter the pending state, a software method of
*   generating the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_SetPending(void)
{
    *IRQ_TF_INTC_SET_PD = IRQ_TF__INTC_MASK;
}


/*******************************************************************************
* Function Name: IRQ_TF_ClearPending
********************************************************************************
*
* Summary:
*   Clears a pending interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void IRQ_TF_ClearPending(void)
{
    *IRQ_TF_INTC_CLR_PD = IRQ_TF__INTC_MASK;
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
