/*******************************************************************************
* File Name: sdlogger_1_sdspi_1_SPI.c
* Version 2.40
*
* Description:
*  This file provides all API functionality of the SPI Master component.
*
* Note:
*  None.
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "sdlogger_1_sdspi_1_SPI_PVT.h"

#if(sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED)
    volatile uint8 sdlogger_1_sdspi_1_SPI_txBuffer[sdlogger_1_sdspi_1_SPI_TX_BUFFER_SIZE] = {0u};
    volatile uint8 sdlogger_1_sdspi_1_SPI_txBufferFull;
    volatile uint8 sdlogger_1_sdspi_1_SPI_txBufferRead;
    volatile uint8 sdlogger_1_sdspi_1_SPI_txBufferWrite;
#endif /* (sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED) */

#if(sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED)
    volatile uint8 sdlogger_1_sdspi_1_SPI_rxBuffer[sdlogger_1_sdspi_1_SPI_RX_BUFFER_SIZE] = {0u};
    volatile uint8 sdlogger_1_sdspi_1_SPI_rxBufferFull;
    volatile uint8 sdlogger_1_sdspi_1_SPI_rxBufferRead;
    volatile uint8 sdlogger_1_sdspi_1_SPI_rxBufferWrite;
#endif /* (sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED) */

uint8 sdlogger_1_sdspi_1_SPI_initVar = 0u;

volatile uint8 sdlogger_1_sdspi_1_SPI_swStatusTx;
volatile uint8 sdlogger_1_sdspi_1_SPI_swStatusRx;


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_Init
********************************************************************************
*
* Summary:
*  Inits/Restores default SPIM configuration provided with customizer.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Side Effects:
*  When this function is called it initializes all of the necessary parameters
*  for execution. i.e. setting the initial interrupt mask, configuring the
*  interrupt service routine, configuring the bit-counter parameters and
*  clearing the FIFO and Status Register.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_Init(void) 
{
    /* Initialize the Bit counter */
    sdlogger_1_sdspi_1_SPI_COUNTER_PERIOD_REG = sdlogger_1_sdspi_1_SPI_BITCTR_INIT;

    /* Init TX ISR  */
    #if(0u != sdlogger_1_sdspi_1_SPI_INTERNAL_TX_INT_ENABLED)
        CyIntDisable         (sdlogger_1_sdspi_1_SPI_TX_ISR_NUMBER);
        CyIntSetPriority     (sdlogger_1_sdspi_1_SPI_TX_ISR_NUMBER,  sdlogger_1_sdspi_1_SPI_TX_ISR_PRIORITY);
        (void) CyIntSetVector(sdlogger_1_sdspi_1_SPI_TX_ISR_NUMBER, &sdlogger_1_sdspi_1_SPI_TX_ISR);
    #endif /* (0u != sdlogger_1_sdspi_1_SPI_INTERNAL_TX_INT_ENABLED) */

    /* Init RX ISR  */
    #if(0u != sdlogger_1_sdspi_1_SPI_INTERNAL_RX_INT_ENABLED)
        CyIntDisable         (sdlogger_1_sdspi_1_SPI_RX_ISR_NUMBER);
        CyIntSetPriority     (sdlogger_1_sdspi_1_SPI_RX_ISR_NUMBER,  sdlogger_1_sdspi_1_SPI_RX_ISR_PRIORITY);
        (void) CyIntSetVector(sdlogger_1_sdspi_1_SPI_RX_ISR_NUMBER, &sdlogger_1_sdspi_1_SPI_RX_ISR);
    #endif /* (0u != sdlogger_1_sdspi_1_SPI_INTERNAL_RX_INT_ENABLED) */

    /* Clear any stray data from the RX and TX FIFO */
    sdlogger_1_sdspi_1_SPI_ClearFIFO();

    #if(sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED)
        sdlogger_1_sdspi_1_SPI_rxBufferFull  = 0u;
        sdlogger_1_sdspi_1_SPI_rxBufferRead  = 0u;
        sdlogger_1_sdspi_1_SPI_rxBufferWrite = 0u;
    #endif /* (sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED) */

    #if(sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED)
        sdlogger_1_sdspi_1_SPI_txBufferFull  = 0u;
        sdlogger_1_sdspi_1_SPI_txBufferRead  = 0u;
        sdlogger_1_sdspi_1_SPI_txBufferWrite = 0u;
    #endif /* (sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED) */

    (void) sdlogger_1_sdspi_1_SPI_ReadTxStatus(); /* Clear Tx status and swStatusTx */
    (void) sdlogger_1_sdspi_1_SPI_ReadRxStatus(); /* Clear Rx status and swStatusRx */

    /* Configure TX and RX interrupt mask */
    sdlogger_1_sdspi_1_SPI_TX_STATUS_MASK_REG = sdlogger_1_sdspi_1_SPI_TX_INIT_INTERRUPTS_MASK;
    sdlogger_1_sdspi_1_SPI_RX_STATUS_MASK_REG = sdlogger_1_sdspi_1_SPI_RX_INIT_INTERRUPTS_MASK;
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_Enable
********************************************************************************
*
* Summary:
*  Enable SPIM component.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_Enable(void) 
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();
    sdlogger_1_sdspi_1_SPI_COUNTER_CONTROL_REG |= sdlogger_1_sdspi_1_SPI_CNTR_ENABLE;
    sdlogger_1_sdspi_1_SPI_TX_STATUS_ACTL_REG  |= sdlogger_1_sdspi_1_SPI_INT_ENABLE;
    sdlogger_1_sdspi_1_SPI_RX_STATUS_ACTL_REG  |= sdlogger_1_sdspi_1_SPI_INT_ENABLE;
    CyExitCriticalSection(enableInterrupts);

    #if(0u != sdlogger_1_sdspi_1_SPI_INTERNAL_CLOCK)
        sdlogger_1_sdspi_1_SPI_IntClock_Enable();
    #endif /* (0u != sdlogger_1_sdspi_1_SPI_INTERNAL_CLOCK) */

    sdlogger_1_sdspi_1_SPI_EnableTxInt();
    sdlogger_1_sdspi_1_SPI_EnableRxInt();
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_Start
********************************************************************************
*
* Summary:
*  Initialize and Enable the SPI Master component.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_initVar - used to check initial configuration, modified on
*  first function call.
*
* Theory:
*  Enable the clock input to enable operation.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_Start(void) 
{
    if(0u == sdlogger_1_sdspi_1_SPI_initVar)
    {
        sdlogger_1_sdspi_1_SPI_Init();
        sdlogger_1_sdspi_1_SPI_initVar = 1u;
    }

    sdlogger_1_sdspi_1_SPI_Enable();
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_Stop
********************************************************************************
*
* Summary:
*  Disable the SPI Master component.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Disable the clock input to enable operation.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_Stop(void) 
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();
    sdlogger_1_sdspi_1_SPI_TX_STATUS_ACTL_REG &= ((uint8) ~sdlogger_1_sdspi_1_SPI_INT_ENABLE);
    sdlogger_1_sdspi_1_SPI_RX_STATUS_ACTL_REG &= ((uint8) ~sdlogger_1_sdspi_1_SPI_INT_ENABLE);
    CyExitCriticalSection(enableInterrupts);

    #if(0u != sdlogger_1_sdspi_1_SPI_INTERNAL_CLOCK)
        sdlogger_1_sdspi_1_SPI_IntClock_Disable();
    #endif /* (0u != sdlogger_1_sdspi_1_SPI_INTERNAL_CLOCK) */

    sdlogger_1_sdspi_1_SPI_DisableTxInt();
    sdlogger_1_sdspi_1_SPI_DisableRxInt();
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_EnableTxInt
********************************************************************************
*
* Summary:
*  Enable internal Tx interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Enable the internal Tx interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_EnableTxInt(void) 
{
    #if(0u != sdlogger_1_sdspi_1_SPI_INTERNAL_TX_INT_ENABLED)
        CyIntEnable(sdlogger_1_sdspi_1_SPI_TX_ISR_NUMBER);
    #endif /* (0u != sdlogger_1_sdspi_1_SPI_INTERNAL_TX_INT_ENABLED) */
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_EnableRxInt
********************************************************************************
*
* Summary:
*  Enable internal Rx interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Enable the internal Rx interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_EnableRxInt(void) 
{
    #if(0u != sdlogger_1_sdspi_1_SPI_INTERNAL_RX_INT_ENABLED)
        CyIntEnable(sdlogger_1_sdspi_1_SPI_RX_ISR_NUMBER);
    #endif /* (0u != sdlogger_1_sdspi_1_SPI_INTERNAL_RX_INT_ENABLED) */
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_DisableTxInt
********************************************************************************
*
* Summary:
*  Disable internal Tx interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Disable the internal Tx interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_DisableTxInt(void) 
{
    #if(0u != sdlogger_1_sdspi_1_SPI_INTERNAL_TX_INT_ENABLED)
        CyIntDisable(sdlogger_1_sdspi_1_SPI_TX_ISR_NUMBER);
    #endif /* (0u != sdlogger_1_sdspi_1_SPI_INTERNAL_TX_INT_ENABLED) */
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_DisableRxInt
********************************************************************************
*
* Summary:
*  Disable internal Rx interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Disable the internal Rx interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_DisableRxInt(void) 
{
    #if(0u != sdlogger_1_sdspi_1_SPI_INTERNAL_RX_INT_ENABLED)
        CyIntDisable(sdlogger_1_sdspi_1_SPI_RX_ISR_NUMBER);
    #endif /* (0u != sdlogger_1_sdspi_1_SPI_INTERNAL_RX_INT_ENABLED) */
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_SetTxInterruptMode
********************************************************************************
*
* Summary:
*  Configure which status bits trigger an interrupt event.
*
* Parameters:
*  intSrc: An or'd combination of the desired status bit masks (defined in the
*  header file).
*
* Return:
*  None.
*
* Theory:
*  Enables the output of specific status bits to the interrupt controller.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_SetTxInterruptMode(uint8 intSrc) 
{
    sdlogger_1_sdspi_1_SPI_TX_STATUS_MASK_REG = intSrc;
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_SetRxInterruptMode
********************************************************************************
*
* Summary:
*  Configure which status bits trigger an interrupt event.
*
* Parameters:
*  intSrc: An or'd combination of the desired status bit masks (defined in the
*  header file).
*
* Return:
*  None.
*
* Theory:
*  Enables the output of specific status bits to the interrupt controller.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_SetRxInterruptMode(uint8 intSrc) 
{
    sdlogger_1_sdspi_1_SPI_RX_STATUS_MASK_REG  = intSrc;
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_ReadTxStatus
********************************************************************************
*
* Summary:
*  Read the Tx status register for the component.
*
* Parameters:
*  None.
*
* Return:
*  Contents of the Tx status register.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_swStatusTx - used to store in software status register,
*  modified every function call - resets to zero.
*
* Theory:
*  Allows the user and the API to read the Tx status register for error
*  detection and flow control.
*
* Side Effects:
*  Clear Tx status register of the component.
*
* Reentrant:
*  No.
*
*******************************************************************************/
uint8 sdlogger_1_sdspi_1_SPI_ReadTxStatus(void) 
{
    uint8 tmpStatus;

    #if(sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED)
        /* Disable TX interrupt to protect global veriables */
        sdlogger_1_sdspi_1_SPI_DisableTxInt();

        tmpStatus = sdlogger_1_sdspi_1_SPI_GET_STATUS_TX(sdlogger_1_sdspi_1_SPI_swStatusTx);
        sdlogger_1_sdspi_1_SPI_swStatusTx = 0u;

        sdlogger_1_sdspi_1_SPI_EnableTxInt();

    #else

        tmpStatus = sdlogger_1_sdspi_1_SPI_TX_STATUS_REG;

    #endif /* (sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED) */

    return(tmpStatus);
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_ReadRxStatus
********************************************************************************
*
* Summary:
*  Read the Rx status register for the component.
*
* Parameters:
*  None.
*
* Return:
*  Contents of the Rx status register.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_swStatusRx - used to store in software Rx status register,
*  modified every function call - resets to zero.
*
* Theory:
*  Allows the user and the API to read the Rx status register for error
*  detection and flow control.
*
* Side Effects:
*  Clear Rx status register of the component.
*
* Reentrant:
*  No.
*
*******************************************************************************/
uint8 sdlogger_1_sdspi_1_SPI_ReadRxStatus(void) 
{
    uint8 tmpStatus;

    #if(sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED)
        /* Disable RX interrupt to protect global veriables */
        sdlogger_1_sdspi_1_SPI_DisableRxInt();

        tmpStatus = sdlogger_1_sdspi_1_SPI_GET_STATUS_RX(sdlogger_1_sdspi_1_SPI_swStatusRx);
        sdlogger_1_sdspi_1_SPI_swStatusRx = 0u;

        sdlogger_1_sdspi_1_SPI_EnableRxInt();

    #else

        tmpStatus = sdlogger_1_sdspi_1_SPI_RX_STATUS_REG;

    #endif /* (sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED) */

    return(tmpStatus);
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_WriteTxData
********************************************************************************
*
* Summary:
*  Write a byte of data to be sent across the SPI.
*
* Parameters:
*  txDataByte: The data value to send across the SPI.
*
* Return:
*  None.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer, modified every function
*  call if TX Software Buffer is used.
*  sdlogger_1_sdspi_1_SPI_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer.
*  sdlogger_1_sdspi_1_SPI_txBuffer[sdlogger_1_sdspi_1_SPI_TX_BUFFER_SIZE] - used to store
*  data to sending, modified every function call if TX Software Buffer is used.
*
* Theory:
*  Allows the user to transmit any byte of data in a single transfer.
*
* Side Effects:
*  If this function is called again before the previous byte is finished then
*  the next byte will be appended to the transfer with no time between
*  the byte transfers. Clear Tx status register of the component.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_WriteTxData(uint8 txData) 
{
    #if(sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED)

        uint8 tempStatus;
        uint8 tmpTxBufferRead;

        /* Block if TX buffer is FULL: don't overwrite */
        do
        {
            tmpTxBufferRead = sdlogger_1_sdspi_1_SPI_txBufferRead;
            if(0u == tmpTxBufferRead)
            {
                tmpTxBufferRead = (sdlogger_1_sdspi_1_SPI_TX_BUFFER_SIZE - 1u);
            }
            else
            {
                tmpTxBufferRead--;
            }

        }while(tmpTxBufferRead == sdlogger_1_sdspi_1_SPI_txBufferWrite);

        /* Disable TX interrupt to protect global veriables */
        sdlogger_1_sdspi_1_SPI_DisableTxInt();

        tempStatus = sdlogger_1_sdspi_1_SPI_GET_STATUS_TX(sdlogger_1_sdspi_1_SPI_swStatusTx);
        sdlogger_1_sdspi_1_SPI_swStatusTx = tempStatus;


        if((sdlogger_1_sdspi_1_SPI_txBufferRead == sdlogger_1_sdspi_1_SPI_txBufferWrite) &&
           (0u != (sdlogger_1_sdspi_1_SPI_swStatusTx & sdlogger_1_sdspi_1_SPI_STS_TX_FIFO_NOT_FULL)))
        {
            /* Add directly to the TX FIFO */
            CY_SET_REG8(sdlogger_1_sdspi_1_SPI_TXDATA_PTR, txData);
        }
        else
        {
            /* Add to the TX software buffer */
            sdlogger_1_sdspi_1_SPI_txBufferWrite++;
            if(sdlogger_1_sdspi_1_SPI_txBufferWrite >= sdlogger_1_sdspi_1_SPI_TX_BUFFER_SIZE)
            {
                sdlogger_1_sdspi_1_SPI_txBufferWrite = 0u;
            }

            if(sdlogger_1_sdspi_1_SPI_txBufferWrite == sdlogger_1_sdspi_1_SPI_txBufferRead)
            {
                sdlogger_1_sdspi_1_SPI_txBufferRead++;
                if(sdlogger_1_sdspi_1_SPI_txBufferRead >= sdlogger_1_sdspi_1_SPI_TX_BUFFER_SIZE)
                {
                    sdlogger_1_sdspi_1_SPI_txBufferRead = 0u;
                }
                sdlogger_1_sdspi_1_SPI_txBufferFull = 1u;
            }

            sdlogger_1_sdspi_1_SPI_txBuffer[sdlogger_1_sdspi_1_SPI_txBufferWrite] = txData;

            sdlogger_1_sdspi_1_SPI_TX_STATUS_MASK_REG |= sdlogger_1_sdspi_1_SPI_STS_TX_FIFO_NOT_FULL;
        }

        sdlogger_1_sdspi_1_SPI_EnableTxInt();

    #else

        while(0u == (sdlogger_1_sdspi_1_SPI_TX_STATUS_REG & sdlogger_1_sdspi_1_SPI_STS_TX_FIFO_NOT_FULL))
        {
            ; /* Wait for room in FIFO */
        }

        /* Put byte in TX FIFO */
        CY_SET_REG8(sdlogger_1_sdspi_1_SPI_TXDATA_PTR, txData);

    #endif /* (sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED) */
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_ReadRxData
********************************************************************************
*
* Summary:
*  Read the next byte of data received across the SPI.
*
* Parameters:
*  None.
*
* Return:
*  The next byte of data read from the FIFO.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer.
*  sdlogger_1_sdspi_1_SPI_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer, modified every function
*  call if RX Software Buffer is used.
*  sdlogger_1_sdspi_1_SPI_rxBuffer[sdlogger_1_sdspi_1_SPI_RX_BUFFER_SIZE] - used to store
*  received data.
*
* Theory:
*  Allows the user to read a byte of data received.
*
* Side Effects:
*  Will return invalid data if the FIFO is empty. The user should Call
*  GetRxBufferSize() and if it returns a non-zero value then it is safe to call
*  ReadByte() function.
*
* Reentrant:
*  No.
*
*******************************************************************************/
uint8 sdlogger_1_sdspi_1_SPI_ReadRxData(void) 
{
    uint8 rxData;

    #if(sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED)

        /* Disable RX interrupt to protect global veriables */
        sdlogger_1_sdspi_1_SPI_DisableRxInt();

        if(sdlogger_1_sdspi_1_SPI_rxBufferRead != sdlogger_1_sdspi_1_SPI_rxBufferWrite)
        {
            if(0u == sdlogger_1_sdspi_1_SPI_rxBufferFull)
            {
                sdlogger_1_sdspi_1_SPI_rxBufferRead++;
                if(sdlogger_1_sdspi_1_SPI_rxBufferRead >= sdlogger_1_sdspi_1_SPI_RX_BUFFER_SIZE)
                {
                    sdlogger_1_sdspi_1_SPI_rxBufferRead = 0u;
                }
            }
            else
            {
                sdlogger_1_sdspi_1_SPI_rxBufferFull = 0u;
            }
        }

        rxData = sdlogger_1_sdspi_1_SPI_rxBuffer[sdlogger_1_sdspi_1_SPI_rxBufferRead];

        sdlogger_1_sdspi_1_SPI_EnableRxInt();

    #else

        rxData = CY_GET_REG8(sdlogger_1_sdspi_1_SPI_RXDATA_PTR);

    #endif /* (sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED) */

    return(rxData);
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_GetRxBufferSize
********************************************************************************
*
* Summary:
*  Returns the number of bytes/words of data currently held in the RX buffer.
*  If RX Software Buffer not used then function return 0 if FIFO empty or 1 if
*  FIFO not empty. In another case function return size of RX Software Buffer.
*
* Parameters:
*  None.
*
* Return:
*  Integer count of the number of bytes/words in the RX buffer.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer.
*  sdlogger_1_sdspi_1_SPI_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer.
*
* Side Effects:
*  Clear status register of the component.
*
*******************************************************************************/
uint8 sdlogger_1_sdspi_1_SPI_GetRxBufferSize(void) 
{
    uint8 size;

    #if(sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED)

        /* Disable RX interrupt to protect global veriables */
        sdlogger_1_sdspi_1_SPI_DisableRxInt();

        if(sdlogger_1_sdspi_1_SPI_rxBufferRead == sdlogger_1_sdspi_1_SPI_rxBufferWrite)
        {
            size = 0u;
        }
        else if(sdlogger_1_sdspi_1_SPI_rxBufferRead < sdlogger_1_sdspi_1_SPI_rxBufferWrite)
        {
            size = (sdlogger_1_sdspi_1_SPI_rxBufferWrite - sdlogger_1_sdspi_1_SPI_rxBufferRead);
        }
        else
        {
            size = (sdlogger_1_sdspi_1_SPI_RX_BUFFER_SIZE - sdlogger_1_sdspi_1_SPI_rxBufferRead) + sdlogger_1_sdspi_1_SPI_rxBufferWrite;
        }

        sdlogger_1_sdspi_1_SPI_EnableRxInt();

    #else

        /* We can only know if there is data in the RX FIFO */
        size = (0u != (sdlogger_1_sdspi_1_SPI_RX_STATUS_REG & sdlogger_1_sdspi_1_SPI_STS_RX_FIFO_NOT_EMPTY)) ? 1u : 0u;

    #endif /* (sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED) */

    return(size);
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_GetTxBufferSize
********************************************************************************
*
* Summary:
*  Returns the number of bytes/words of data currently held in the TX buffer.
*  If TX Software Buffer not used then function return 0 - if FIFO empty, 1 - if
*  FIFO not full, 4 - if FIFO full. In another case function return size of TX
*  Software Buffer.
*
* Parameters:
*  None.
*
* Return:
*  Integer count of the number of bytes/words in the TX buffer.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer.
*  sdlogger_1_sdspi_1_SPI_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer.
*
* Side Effects:
*  Clear status register of the component.
*
*******************************************************************************/
uint8  sdlogger_1_sdspi_1_SPI_GetTxBufferSize(void) 
{
    uint8 size;

    #if(sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED)
        /* Disable TX interrupt to protect global veriables */
        sdlogger_1_sdspi_1_SPI_DisableTxInt();

        if(sdlogger_1_sdspi_1_SPI_txBufferRead == sdlogger_1_sdspi_1_SPI_txBufferWrite)
        {
            size = 0u;
        }
        else if(sdlogger_1_sdspi_1_SPI_txBufferRead < sdlogger_1_sdspi_1_SPI_txBufferWrite)
        {
            size = (sdlogger_1_sdspi_1_SPI_txBufferWrite - sdlogger_1_sdspi_1_SPI_txBufferRead);
        }
        else
        {
            size = (sdlogger_1_sdspi_1_SPI_TX_BUFFER_SIZE - sdlogger_1_sdspi_1_SPI_txBufferRead) + sdlogger_1_sdspi_1_SPI_txBufferWrite;
        }

        sdlogger_1_sdspi_1_SPI_EnableTxInt();

    #else

        size = sdlogger_1_sdspi_1_SPI_TX_STATUS_REG;

        if(0u != (size & sdlogger_1_sdspi_1_SPI_STS_TX_FIFO_EMPTY))
        {
            size = 0u;
        }
        else if(0u != (size & sdlogger_1_sdspi_1_SPI_STS_TX_FIFO_NOT_FULL))
        {
            size = 1u;
        }
        else
        {
            size = sdlogger_1_sdspi_1_SPI_FIFO_SIZE;
        }

    #endif /* (sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED) */

    return(size);
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_ClearRxBuffer
********************************************************************************
*
* Summary:
*  Clear the RX RAM buffer by setting the read and write pointers both to zero.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer, modified every function
*  call - resets to zero.
*  sdlogger_1_sdspi_1_SPI_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer, modified every function call -
*  resets to zero.
*
* Theory:
*  Setting the pointers to zero makes the system believe there is no data to
*  read and writing will resume at address 0 overwriting any data that may have
*  remained in the RAM.
*
* Side Effects:
*  Any received data not read from the RAM buffer will be lost when overwritten.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_ClearRxBuffer(void) 
{
    /* Clear Hardware RX FIFO */
    while(0u !=(sdlogger_1_sdspi_1_SPI_RX_STATUS_REG & sdlogger_1_sdspi_1_SPI_STS_RX_FIFO_NOT_EMPTY))
    {
        (void) CY_GET_REG8(sdlogger_1_sdspi_1_SPI_RXDATA_PTR);
    }

    #if(sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED)
        /* Disable RX interrupt to protect global veriables */
        sdlogger_1_sdspi_1_SPI_DisableRxInt();

        sdlogger_1_sdspi_1_SPI_rxBufferFull  = 0u;
        sdlogger_1_sdspi_1_SPI_rxBufferRead  = 0u;
        sdlogger_1_sdspi_1_SPI_rxBufferWrite = 0u;

        sdlogger_1_sdspi_1_SPI_EnableRxInt();
    #endif /* (sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED) */
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_ClearTxBuffer
********************************************************************************
*
* Summary:
*  Clear the TX RAM buffer by setting the read and write pointers both to zero.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer, modified every function
*  call - resets to zero.
*  sdlogger_1_sdspi_1_SPI_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer, modified every function call -
*  resets to zero.
*
* Theory:
*  Setting the pointers to zero makes the system believe there is no data to
*  read and writing will resume at address 0 overwriting any data that may have
*  remained in the RAM.
*
* Side Effects:
*  Any data not yet transmitted from the RAM buffer will be lost when
*  overwritten.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_ClearTxBuffer(void) 
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();
    /* Clear TX FIFO */
    sdlogger_1_sdspi_1_SPI_AUX_CONTROL_DP0_REG |= ((uint8)  sdlogger_1_sdspi_1_SPI_TX_FIFO_CLR);
    sdlogger_1_sdspi_1_SPI_AUX_CONTROL_DP0_REG &= ((uint8) ~sdlogger_1_sdspi_1_SPI_TX_FIFO_CLR);

    #if(sdlogger_1_sdspi_1_SPI_USE_SECOND_DATAPATH)
        /* Clear TX FIFO for 2nd Datapath */
        sdlogger_1_sdspi_1_SPI_AUX_CONTROL_DP1_REG |= ((uint8)  sdlogger_1_sdspi_1_SPI_TX_FIFO_CLR);
        sdlogger_1_sdspi_1_SPI_AUX_CONTROL_DP1_REG &= ((uint8) ~sdlogger_1_sdspi_1_SPI_TX_FIFO_CLR);
    #endif /* (sdlogger_1_sdspi_1_SPI_USE_SECOND_DATAPATH) */
    CyExitCriticalSection(enableInterrupts);

    #if(sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED)
        /* Disable TX interrupt to protect global veriables */
        sdlogger_1_sdspi_1_SPI_DisableTxInt();

        sdlogger_1_sdspi_1_SPI_txBufferFull  = 0u;
        sdlogger_1_sdspi_1_SPI_txBufferRead  = 0u;
        sdlogger_1_sdspi_1_SPI_txBufferWrite = 0u;

        /* Buffer is EMPTY: disable TX FIFO NOT FULL interrupt */
        sdlogger_1_sdspi_1_SPI_TX_STATUS_MASK_REG &= ((uint8) ~sdlogger_1_sdspi_1_SPI_STS_TX_FIFO_NOT_FULL);

        sdlogger_1_sdspi_1_SPI_EnableTxInt();
    #endif /* (sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED) */
}


#if(0u != sdlogger_1_sdspi_1_SPI_BIDIRECTIONAL_MODE)
    /*******************************************************************************
    * Function Name: sdlogger_1_sdspi_1_SPI_TxEnable
    ********************************************************************************
    *
    * Summary:
    *  If the SPI master is configured to use a single bi-directional pin then this
    *  will set the bi-directional pin to transmit.
    *
    * Parameters:
    *  None.
    *
    * Return:
    *  None.
    *
    *******************************************************************************/
    void sdlogger_1_sdspi_1_SPI_TxEnable(void) 
    {
        sdlogger_1_sdspi_1_SPI_CONTROL_REG |= sdlogger_1_sdspi_1_SPI_CTRL_TX_SIGNAL_EN;
    }


    /*******************************************************************************
    * Function Name: sdlogger_1_sdspi_1_SPI_TxDisable
    ********************************************************************************
    *
    * Summary:
    *  If the SPI master is configured to use a single bi-directional pin then this
    *  will set the bi-directional pin to receive.
    *
    * Parameters:
    *  None.
    *
    * Return:
    *  None.
    *
    *******************************************************************************/
    void sdlogger_1_sdspi_1_SPI_TxDisable(void) 
    {
        sdlogger_1_sdspi_1_SPI_CONTROL_REG &= ((uint8) ~sdlogger_1_sdspi_1_SPI_CTRL_TX_SIGNAL_EN);
    }

#endif /* (0u != sdlogger_1_sdspi_1_SPI_BIDIRECTIONAL_MODE) */


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_PutArray
********************************************************************************
*
* Summary:
*  Write available data from ROM/RAM to the TX buffer while space is available
*  in the TX buffer. Keep trying until all data is passed to the TX buffer.
*
* Parameters:
*  *buffer: Pointer to the location in RAM containing the data to send
*  byteCount: The number of bytes to move to the transmit buffer.
*
* Return:
*  None.
*
* Side Effects:
*  Will stay in this routine until all data has been sent.  May get locked in
*  this loop if data is not being initiated by the master if there is not
*  enough room in the TX FIFO.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_PutArray(const uint8 buffer[], uint8 byteCount)
                                                                          
{
    uint8 bufIndex;

    bufIndex = 0u;

    while(byteCount > 0u)
    {
        sdlogger_1_sdspi_1_SPI_WriteTxData(buffer[bufIndex]);
        bufIndex++;
        byteCount--;
    }
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_ClearFIFO
********************************************************************************
*
* Summary:
*  Clear the RX and TX FIFO's of all data for a fresh start.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Side Effects:
*  Clear status register of the component.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_ClearFIFO(void) 
{
    uint8 enableInterrupts;

    /* Clear Hardware RX FIFO */
    while(0u !=(sdlogger_1_sdspi_1_SPI_RX_STATUS_REG & sdlogger_1_sdspi_1_SPI_STS_RX_FIFO_NOT_EMPTY))
    {
        (void) CY_GET_REG8(sdlogger_1_sdspi_1_SPI_RXDATA_PTR);
    }

    enableInterrupts = CyEnterCriticalSection();
    /* Clear TX FIFO */
    sdlogger_1_sdspi_1_SPI_AUX_CONTROL_DP0_REG |= ((uint8)  sdlogger_1_sdspi_1_SPI_TX_FIFO_CLR);
    sdlogger_1_sdspi_1_SPI_AUX_CONTROL_DP0_REG &= ((uint8) ~sdlogger_1_sdspi_1_SPI_TX_FIFO_CLR);

    #if(sdlogger_1_sdspi_1_SPI_USE_SECOND_DATAPATH)
        /* Clear TX FIFO for 2nd Datapath */
        sdlogger_1_sdspi_1_SPI_AUX_CONTROL_DP1_REG |= ((uint8)  sdlogger_1_sdspi_1_SPI_TX_FIFO_CLR);
        sdlogger_1_sdspi_1_SPI_AUX_CONTROL_DP1_REG &= ((uint8) ~sdlogger_1_sdspi_1_SPI_TX_FIFO_CLR);
    #endif /* (sdlogger_1_sdspi_1_SPI_USE_SECOND_DATAPATH) */
    CyExitCriticalSection(enableInterrupts);
}


/* Following functions are for version Compatibility, they are obsolete.
*  Please do not use it in new projects.
*/


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_EnableInt
********************************************************************************
*
* Summary:
*  Enable internal interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Enable the internal interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_EnableInt(void) 
{
    sdlogger_1_sdspi_1_SPI_EnableRxInt();
    sdlogger_1_sdspi_1_SPI_EnableTxInt();
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_DisableInt
********************************************************************************
*
* Summary:
*  Disable internal interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Disable the internal interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_DisableInt(void) 
{
    sdlogger_1_sdspi_1_SPI_DisableTxInt();
    sdlogger_1_sdspi_1_SPI_DisableRxInt();
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_SetInterruptMode
********************************************************************************
*
* Summary:
*  Configure which status bits trigger an interrupt event.
*
* Parameters:
*  intSrc: An or'd combination of the desired status bit masks (defined in the
*  header file).
*
* Return:
*  None.
*
* Theory:
*  Enables the output of specific status bits to the interrupt controller.
*
*******************************************************************************/
void sdlogger_1_sdspi_1_SPI_SetInterruptMode(uint8 intSrc) 
{
    sdlogger_1_sdspi_1_SPI_TX_STATUS_MASK_REG  = (intSrc & ((uint8) ~sdlogger_1_sdspi_1_SPI_STS_SPI_IDLE));
    sdlogger_1_sdspi_1_SPI_RX_STATUS_MASK_REG  =  intSrc;
}


/*******************************************************************************
* Function Name: sdlogger_1_sdspi_1_SPI_ReadStatus
********************************************************************************
*
* Summary:
*  Read the status register for the component.
*
* Parameters:
*  None.
*
* Return:
*  Contents of the status register.
*
* Global variables:
*  sdlogger_1_sdspi_1_SPI_swStatus - used to store in software status register,
*  modified every function call - resets to zero.
*
* Theory:
*  Allows the user and the API to read the status register for error detection
*  and flow control.
*
* Side Effects:
*  Clear status register of the component.
*
* Reentrant:
*  No.
*
*******************************************************************************/
uint8 sdlogger_1_sdspi_1_SPI_ReadStatus(void) 
{
    uint8 tmpStatus;

    #if(sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED || sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED)

        sdlogger_1_sdspi_1_SPI_DisableInt();

        tmpStatus  = sdlogger_1_sdspi_1_SPI_GET_STATUS_RX(sdlogger_1_sdspi_1_SPI_swStatusRx);
        tmpStatus |= sdlogger_1_sdspi_1_SPI_GET_STATUS_TX(sdlogger_1_sdspi_1_SPI_swStatusTx);
        tmpStatus &= ((uint8) ~sdlogger_1_sdspi_1_SPI_STS_SPI_IDLE);

        sdlogger_1_sdspi_1_SPI_swStatusTx = 0u;
        sdlogger_1_sdspi_1_SPI_swStatusRx = 0u;

        sdlogger_1_sdspi_1_SPI_EnableInt();

    #else

        tmpStatus  = sdlogger_1_sdspi_1_SPI_RX_STATUS_REG;
        tmpStatus |= sdlogger_1_sdspi_1_SPI_TX_STATUS_REG;
        tmpStatus &= ((uint8) ~sdlogger_1_sdspi_1_SPI_STS_SPI_IDLE);

    #endif /* (sdlogger_1_sdspi_1_SPI_TX_SOFTWARE_BUF_ENABLED || sdlogger_1_sdspi_1_SPI_RX_SOFTWARE_BUF_ENABLED) */

    return(tmpStatus);
}


/* [] END OF FILE */
