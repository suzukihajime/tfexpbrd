/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"

static char btmodule_1_rxfifo_fifo[btmodule_1_rxfifo_FIFO_SIZE];
static int32 btmodule_1_rxfifo_rptr = 0, btmodule_1_rxfifo_wptr = 0;

void btmodule_1_rxfifo_Start(void)
{
	btmodule_1_rxfifo_Clear(1);
	return;
}

void btmodule_1_rxfifo_Stop(void)
{
	/* nothing to do */
	return;
}

inline char btmodule_1_rxfifo_PopChar(void)
{
	register char c;
	c = btmodule_1_rxfifo_fifo[btmodule_1_rxfifo_rptr];
	btmodule_1_rxfifo_rptr = (btmodule_1_rxfifo_rptr+1) & (btmodule_1_rxfifo_FIFO_SIZE-1);
	return c;
}

/* 境界をまたぐとうまく動かない実装!!!!! */
inline char* btmodule_1_rxfifo_PopString(void)
{
	char *ptr = &btmodule_1_rxfifo_fifo[btmodule_1_rxfifo_rptr];
	btmodule_1_rxfifo_PushChar('\0');
	btmodule_1_rxfifo_rptr = btmodule_1_rxfifo_wptr;
	return(ptr);
}

inline void btmodule_1_rxfifo_PushChar(char c)
{
	btmodule_1_rxfifo_fifo[btmodule_1_rxfifo_wptr] = c;
	btmodule_1_rxfifo_wptr = (btmodule_1_rxfifo_wptr+1) & (btmodule_1_rxfifo_FIFO_SIZE-1);
	return;
}

inline void btmodule_1_rxfifo_PushString(char *str)
{
	char c;
	while((c = *str++)) {
		btmodule_1_rxfifo_PushChar(c);
	}
	return;
}

inline uint16 btmodule_1_rxfifo_GetContentSize(void)
{
	return((btmodule_1_rxfifo_wptr - btmodule_1_rxfifo_rptr) % btmodule_1_rxfifo_FIFO_SIZE);
}

inline uint16 btmodule_1_rxfifo_GetVacancySize(void)
{
	int32 offset;
	offset = (btmodule_1_rxfifo_wptr >= btmodule_1_rxfifo_rptr) ? btmodule_1_rxfifo_FIFO_SIZE : 0;
	return(btmodule_1_rxfifo_rptr + offset - btmodule_1_rxfifo_wptr);
}

inline void btmodule_1_rxfifo_Clear(char bufc)
{
	int i;
	btmodule_1_rxfifo_rptr = 0;
	btmodule_1_rxfifo_wptr = 0;
	if(bufc) {
		for(i = 0; i < btmodule_1_rxfifo_FIFO_SIZE; i++) {
			btmodule_1_rxfifo_fifo[i] = '\0';
		}
	}
	return;
}

/* [] END OF FILE */
