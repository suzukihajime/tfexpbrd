/*******************************************************************************
* File Name: IRQ_TF.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_IRQ_TF_H)
#define CY_ISR_IRQ_TF_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void IRQ_TF_Start(void);
void IRQ_TF_StartEx(cyisraddress address);
void IRQ_TF_Stop(void);

CY_ISR_PROTO(IRQ_TF_Interrupt);

void IRQ_TF_SetVector(cyisraddress address);
cyisraddress IRQ_TF_GetVector(void);

void IRQ_TF_SetPriority(uint8 priority);
uint8 IRQ_TF_GetPriority(void);

void IRQ_TF_Enable(void);
uint8 IRQ_TF_GetState(void);
void IRQ_TF_Disable(void);

void IRQ_TF_SetPending(void);
void IRQ_TF_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the IRQ_TF ISR. */
#define IRQ_TF_INTC_VECTOR            ((reg32 *) IRQ_TF__INTC_VECT)

/* Address of the IRQ_TF ISR priority. */
#define IRQ_TF_INTC_PRIOR             ((reg8 *) IRQ_TF__INTC_PRIOR_REG)

/* Priority of the IRQ_TF interrupt. */
#define IRQ_TF_INTC_PRIOR_NUMBER      IRQ_TF__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable IRQ_TF interrupt. */
#define IRQ_TF_INTC_SET_EN            ((reg32 *) IRQ_TF__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the IRQ_TF interrupt. */
#define IRQ_TF_INTC_CLR_EN            ((reg32 *) IRQ_TF__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the IRQ_TF interrupt state to pending. */
#define IRQ_TF_INTC_SET_PD            ((reg32 *) IRQ_TF__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the IRQ_TF interrupt. */
#define IRQ_TF_INTC_CLR_PD            ((reg32 *) IRQ_TF__INTC_CLR_PD_REG)


#endif /* CY_ISR_IRQ_TF_H */


/* [] END OF FILE */
