/*******************************************************************************
 * File Name: project.h  
 * PSoC Creator 2.2
 *
 *  Description:
 *  This file is automatically generated by PSoC Creator and should not 
 *  be edited by hand.
 *
 *
 ********************************************************************************
 * Copyright 2008-2011, Cypress Semiconductor Corporation.  All rights reserved.
 * You may use this file only in accordance with the license, terms, conditions, 
 * disclaimers, and limitations in the end user license agreement accompanying 
 * the software package with which this file was provided.
 ********************************************************************************/
#include <cyfitter_cfg.h>
#include <cydevice.h>
#include <cydevice_trm.h>
#include <cyfitter.h>
#include <UART_1.h>
#include <CD_aliases.h>
#include <CD.h>
#include <QuadDec_1.h>
#include <MOSI_aliases.h>
#include <MOSI.h>
#include <SCK_aliases.h>
#include <SCK.h>
#include <CS_aliases.h>
#include <CS.h>
#include <MISO_aliases.h>
#include <MISO.h>
#include <Clock_1.h>
#include <Clock_2.h>
#include <unused1_aliases.h>
#include <unused1.h>
#include <unused2_aliases.h>
#include <unused2.h>
#include <sdlogger_1_sdlogger.h>
#include <ENC_PH_A_aliases.h>
#include <ENC_PH_A.h>
#include <ENC_PH_B_aliases.h>
#include <ENC_PH_B.h>
#include <Clock_3.h>
#include <UART_TF.h>
#include <IRQ_TF.h>
#include <LED_R_aliases.h>
#include <LED_R.h>
#include <LED_Y1_aliases.h>
#include <LED_Y1.h>
#include <LED_Y2_aliases.h>
#include <LED_Y2.h>
#include <LED_G_aliases.h>
#include <LED_G.h>
#include <buzzer_1_buzzer.h>
#include <Neg_aliases.h>
#include <Neg.h>
#include <Pos_aliases.h>
#include <Pos.h>
#include <btmodule_1_btmodule.h>
#include <CTS_BT_aliases.h>
#include <CTS_BT.h>
#include <RTS_BT_aliases.h>
#include <RTS_BT.h>
#include <RESET_BT_aliases.h>
#include <RESET_BT.h>
#include <UART_1_IntClock.h>
#include <QuadDec_1_Cnt16.h>
#include <sdlogger_1_sdspi_1_sdspi.h>
#include <sdlogger_1_sdspi_1_diskio.h>
#include <sdlogger_1_sdspi_1_integer.h>
#include <sdlogger_1_fatfs_1_ff.h>
#include <sdlogger_1_fatfs_1_ffconf.h>
#include <sdlogger_1_fatfs_1_integer.h>
#include <sdlogger_1_fatfs_1_diskio.h>
#include <sdlogger_1_irq_write.h>
#include <sdlogger_1_irq.h>
#include <UART_TF_IntClock.h>
#include <buzzer_1_Counter.h>
#include <buzzer_1_Control_Reg.h>
#include <buzzer_1_DMA_dma.h>
#include <btmodule_1_UART.h>
#include <btmodule_1_IRQ_TX.h>
#include <btmodule_1_IRQ_RX.h>
#include <btmodule_1_rxfifo_softwarefifo.h>
#include <btmodule_1_Control_Reg.h>
#include <btmodule_1_txfifo_softwarefifo.h>
#include <sdlogger_1_sdspi_1_SPI.h>
#include <sdlogger_1_sdspi_1_SPI_PVT.h>
#include <sdlogger_1_sdspi_1_CS_Control.h>
#include <sdlogger_1_sdspi_1_CD_Status.h>
#include <sdlogger_1_sdspi_1_Timer_IRQ.h>
#include <btmodule_1_UART_IntClock.h>
#include <core_cm3_psoc5.h>
#include <core_cm3.h>
#include <CyDmac.h>
#include <CyFlash.h>
#include <CyLib.h>
#include <cypins.h>
#include <cyPm.h>
#include <CySpc.h>
#include <cytypes.h>

/*[]*/
