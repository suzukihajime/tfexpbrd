/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"

/* Variable declarations for DMA_Buzzer */
/* Move these variable declarations to the top of the function */
uint8 `$INSTANCE_NAME`_Chan;
uint8 `$INSTANCE_NAME`_TD[2];

uint8 g_buzzbuf[8];
uint8 g_zero = 0;

void `$INSTANCE_NAME`_Start(void)
{
	`$INSTANCE_NAME`_Counter_Start();
	
	/*
	DMA init
	*/
	`$INSTANCE_NAME`_Chan = `$INSTANCE_NAME`_DMA_DmaInitialize(`$INSTANCE_NAME`_DMA_BYTES_PER_BURST,
		`$INSTANCE_NAME`_DMA_REQUEST_PER_BURST, 
		HI16(`$INSTANCE_NAME`_DMA_SRC_BASE), HI16(`$INSTANCE_NAME`_DMA_DST_BASE));
	`$INSTANCE_NAME`_TD[0] = CyDmaTdAllocate();
	`$INSTANCE_NAME`_TD[1] = CyDmaTdAllocate();
	return;
}


void `$INSTANCE_NAME`_Run(uint8 config, uint8 loop)
{
	int i;
	CyDmaChDisable(`$INSTANCE_NAME`_Chan);
	CyDmaClearPendingDrq(`$INSTANCE_NAME`_Chan);
	for(i = 0; i < 8; i++) {
		g_buzzbuf[i] = (config&0x80) ? 0xff : 0;
		config<<=1;
	}
	if(loop == `$INSTANCE_NAME`_CONTINUOUS) {
		CyDmaTdSetConfiguration(`$INSTANCE_NAME`_TD[0], 8, `$INSTANCE_NAME`_TD[0], TD_INC_SRC_ADR/* | TD_AUTO_EXEC_NEXT*/);
	} else {
		CyDmaTdSetConfiguration(`$INSTANCE_NAME`_TD[0], 8, `$INSTANCE_NAME`_TD[1], TD_INC_SRC_ADR/* | TD_AUTO_EXEC_NEXT*/);
	}
	CyDmaTdSetConfiguration(`$INSTANCE_NAME`_TD[1], 1, `$INSTANCE_NAME`_TD[1], TD_INC_SRC_ADR/* | TD_AUTO_EXEC_NEXT*/);
	CyDmaTdSetAddress(`$INSTANCE_NAME`_TD[0], LO16((uint32)g_buzzbuf), LO16((uint32)`$INSTANCE_NAME`_Control_Reg_Control_PTR));
	CyDmaTdSetAddress(`$INSTANCE_NAME`_TD[1], LO16((uint32)g_zero), LO16((uint32)`$INSTANCE_NAME`_Control_Reg_Control_PTR));
	CyDmaChSetInitialTd(`$INSTANCE_NAME`_Chan, `$INSTANCE_NAME`_TD[0]);
	CyDmaChEnable(`$INSTANCE_NAME`_Chan, 1);
	return;
}


void `$INSTANCE_NAME`_Stop(void)
{
	`$INSTANCE_NAME`_Counter_Stop();
	return;
}


/* [] END OF FILE */
