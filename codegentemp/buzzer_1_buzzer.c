/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"

/* Variable declarations for DMA_Buzzer */
/* Move these variable declarations to the top of the function */
uint8 buzzer_1_Chan;
uint8 buzzer_1_TD[2];

uint8 g_buzzbuf[8];
uint8 g_zero = 0;

void buzzer_1_Start(void)
{
	buzzer_1_Counter_Start();
	
	/*
	DMA init
	*/
	buzzer_1_Chan = buzzer_1_DMA_DmaInitialize(buzzer_1_DMA_BYTES_PER_BURST,
		buzzer_1_DMA_REQUEST_PER_BURST, 
		HI16(buzzer_1_DMA_SRC_BASE), HI16(buzzer_1_DMA_DST_BASE));
	buzzer_1_TD[0] = CyDmaTdAllocate();
	buzzer_1_TD[1] = CyDmaTdAllocate();
	return;
}


void buzzer_1_Run(uint8 config, uint8 loop)
{
	int i;
	CyDmaChDisable(buzzer_1_Chan);
	CyDmaClearPendingDrq(buzzer_1_Chan);
	for(i = 0; i < 8; i++) {
		g_buzzbuf[i] = (config&0x80) ? 0xff : 0;
		config<<=1;
	}
	if(loop == buzzer_1_CONTINUOUS) {
		CyDmaTdSetConfiguration(buzzer_1_TD[0], 8, buzzer_1_TD[0], TD_INC_SRC_ADR/* | TD_AUTO_EXEC_NEXT*/);
	} else {
		CyDmaTdSetConfiguration(buzzer_1_TD[0], 8, buzzer_1_TD[1], TD_INC_SRC_ADR/* | TD_AUTO_EXEC_NEXT*/);
	}
	CyDmaTdSetConfiguration(buzzer_1_TD[1], 1, buzzer_1_TD[1], TD_INC_SRC_ADR/* | TD_AUTO_EXEC_NEXT*/);
	CyDmaTdSetAddress(buzzer_1_TD[0], LO16((uint32)g_buzzbuf), LO16((uint32)buzzer_1_Control_Reg_Control_PTR));
	CyDmaTdSetAddress(buzzer_1_TD[1], LO16((uint32)g_zero), LO16((uint32)buzzer_1_Control_Reg_Control_PTR));
	CyDmaChSetInitialTd(buzzer_1_Chan, buzzer_1_TD[0]);
	CyDmaChEnable(buzzer_1_Chan, 1);
	return;
}


void buzzer_1_Stop(void)
{
	buzzer_1_Counter_Stop();
	return;
}


/* [] END OF FILE */
