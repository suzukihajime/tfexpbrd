/*******************************************************************************
* File Name: RESET_BT.c  
* Version 1.80
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "RESET_BT.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 RESET_BT__PORT == 15 && ((RESET_BT__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: RESET_BT_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void RESET_BT_Write(uint8 value) 
{
    uint8 staticBits = (RESET_BT_DR & (uint8)(~RESET_BT_MASK));
    RESET_BT_DR = staticBits | ((uint8)(value << RESET_BT_SHIFT) & RESET_BT_MASK);
}


/*******************************************************************************
* Function Name: RESET_BT_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void RESET_BT_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(RESET_BT_0, mode);
}


/*******************************************************************************
* Function Name: RESET_BT_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro RESET_BT_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 RESET_BT_Read(void) 
{
    return (RESET_BT_PS & RESET_BT_MASK) >> RESET_BT_SHIFT;
}


/*******************************************************************************
* Function Name: RESET_BT_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 RESET_BT_ReadDataReg(void) 
{
    return (RESET_BT_DR & RESET_BT_MASK) >> RESET_BT_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(RESET_BT_INTSTAT) 

    /*******************************************************************************
    * Function Name: RESET_BT_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 RESET_BT_ClearInterrupt(void) 
    {
        return (RESET_BT_INTSTAT & RESET_BT_MASK) >> RESET_BT_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
