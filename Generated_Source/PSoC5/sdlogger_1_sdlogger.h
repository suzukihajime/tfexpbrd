/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

typedef enum {
	sdlogger_1_OK = 0,
	sdlogger_1_NODISK,
	sdlogger_1_ERROR
} sdlogger_1_RESULT;

#define	sdlogger_1_AVAILABLE		0x01			// SDが挿入されている
#define	sdlogger_1_LOGGING		0x02			// ログ取得中

#define	sdlogger_1_WBUF_LENGTH	4096			// 4096バイトたまったら書き込み

#define	sdlogger_1_FILENAME		"LOG%03d.DAT"	// そのままsprintfに渡す

int sdlogger_1_Start(void);
int sdlogger_1_Stop(void);
int sdlogger_1_CreateLog(void);
int sdlogger_1_CloseLog(void);
/* 最低限の書き込み関数 */
int sdlogger_1_Write(char *str, int len);
int sdlogger_1_WriteLine(char *str, int len);
/* sylphide形式のデータを書く関数を作るといいと思うが、sylphide形式はよく分からない */


/* status取得 */
int sdlogger_1_GetStatus(void);
void sdlogger_1_UpdateStatus(void);

/* ファイル名取得 */
char *sdlogger_1_GetFileName(void);
char sdlogger_1_Find(char *filename);

//[] END OF FILE
