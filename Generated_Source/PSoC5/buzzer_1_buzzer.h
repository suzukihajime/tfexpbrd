/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

/* DMA Configuration for DMA_Buzzer */
#define buzzer_1_DMA_BYTES_PER_BURST 1
#define buzzer_1_DMA_REQUEST_PER_BURST 1
#define buzzer_1_DMA_SRC_BASE (CYDEV_SRAM_BASE)
#define buzzer_1_DMA_DST_BASE (CYDEV_PERIPH_BASE)

#define	buzzer_1_CONTINUOUS		(1)
#define	buzzer_1_ONESHOT		(0)


void buzzer_1_Start(void);
void buzzer_1_Run(uint8, uint8);
void buzzer_1_Stop(void);
//[] END OF FILE
