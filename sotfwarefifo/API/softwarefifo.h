/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

#define	`$INSTANCE_NAME`_FIFO_SIZE		(1024)

void `$INSTANCE_NAME`_Start(void);
void `$INSTANCE_NAME`_Stop(void);
inline char `$INSTANCE_NAME`_PopChar(void);
inline char* `$INSTANCE_NAME`_PopString(void);
inline void `$INSTANCE_NAME`_PushChar(char c);
inline void `$INSTANCE_NAME`_PushString(char *str);
inline uint16 `$INSTANCE_NAME`_GetContentSize(void);
inline uint16 `$INSTANCE_NAME`_GetVacancySize(void);
inline void `$INSTANCE_NAME`_Clear(char bufc);
//[] END OF FILE
