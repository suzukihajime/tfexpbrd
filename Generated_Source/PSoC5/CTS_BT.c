/*******************************************************************************
* File Name: CTS_BT.c  
* Version 1.80
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "CTS_BT.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 CTS_BT__PORT == 15 && ((CTS_BT__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: CTS_BT_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void CTS_BT_Write(uint8 value) 
{
    uint8 staticBits = (CTS_BT_DR & (uint8)(~CTS_BT_MASK));
    CTS_BT_DR = staticBits | ((uint8)(value << CTS_BT_SHIFT) & CTS_BT_MASK);
}


/*******************************************************************************
* Function Name: CTS_BT_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void CTS_BT_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(CTS_BT_0, mode);
}


/*******************************************************************************
* Function Name: CTS_BT_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro CTS_BT_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 CTS_BT_Read(void) 
{
    return (CTS_BT_PS & CTS_BT_MASK) >> CTS_BT_SHIFT;
}


/*******************************************************************************
* Function Name: CTS_BT_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 CTS_BT_ReadDataReg(void) 
{
    return (CTS_BT_DR & CTS_BT_MASK) >> CTS_BT_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(CTS_BT_INTSTAT) 

    /*******************************************************************************
    * Function Name: CTS_BT_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 CTS_BT_ClearInterrupt(void) 
    {
        return (CTS_BT_INTSTAT & CTS_BT_MASK) >> CTS_BT_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
