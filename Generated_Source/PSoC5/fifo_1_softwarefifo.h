/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

#define	fifo_1_FIFO_SIZE		(1024)

void fifo_1_Start(void);
void fifo_1_Stop(void);
inline char fifo_1_PopChar(void);
inline void fifo_1_PushChar(char c);
inline void fifo_1_PushString(char *str);
inline uint16 fifo_1_GetContentSize(void);
inline uint16 fifo_1_GetVacancySize(void);
inline void fifo_1_Clear(void);
//[] END OF FILE
