/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

#define	btmodule_1_txfifo_FIFO_SIZE		(1024)

void btmodule_1_txfifo_Start(void);
void btmodule_1_txfifo_Stop(void);
inline char btmodule_1_txfifo_PopChar(void);
inline char* btmodule_1_txfifo_PopString(void);
inline void btmodule_1_txfifo_PushChar(char c);
inline void btmodule_1_txfifo_PushString(char *str);
inline uint16 btmodule_1_txfifo_GetContentSize(void);
inline uint16 btmodule_1_txfifo_GetVacancySize(void);
inline void btmodule_1_txfifo_Clear(char bufc);
//[] END OF FILE
