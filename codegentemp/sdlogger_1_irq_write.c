/*******************************************************************************
* File Name: sdlogger_1_irq_write.c  
* Version 1.70
*
*  Description:
*   API for controlling the state of an interrupt.
*
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/


#include <CYDEVICE_TRM.H>
#include <CYLIB.H>
#include <sdlogger_1_irq_write.H>

#if !defined(sdlogger_1_irq_write__REMOVED) /* Check for removal by optimization */

/*******************************************************************************
*  Place your includes, defines and code here 
********************************************************************************/
/* `#START sdlogger_1_irq_write_intc` */

/* `#END` */

#ifndef CYINT_IRQ_BASE
#define CYINT_IRQ_BASE      16
#endif /* CYINT_IRQ_BASE */
#ifndef CYINT_VECT_TABLE
#define CYINT_VECT_TABLE    ((cyisraddress **) CYREG_NVIC_VECT_OFFSET)
#endif /* CYINT_VECT_TABLE */

/* Declared in startup, used to set unused interrupts to. */
CY_ISR_PROTO(IntDefaultHandler);


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_Start
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   None
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_Start(void)
{
    /* For all we know the interrupt is active. */
    sdlogger_1_irq_write_Disable();

    /* Set the ISR to point to the sdlogger_1_irq_write Interrupt. */
    sdlogger_1_irq_write_SetVector(&sdlogger_1_irq_write_Interrupt);

    /* Set the priority. */
    sdlogger_1_irq_write_SetPriority((uint8)sdlogger_1_irq_write_INTC_PRIOR_NUMBER);

    /* Enable it. */
    sdlogger_1_irq_write_Enable();
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_StartEx
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_StartEx(cyisraddress address)
{
    /* For all we know the interrupt is active. */
    sdlogger_1_irq_write_Disable();

    /* Set the ISR to point to the sdlogger_1_irq_write Interrupt. */
    sdlogger_1_irq_write_SetVector(address);

    /* Set the priority. */
    sdlogger_1_irq_write_SetPriority((uint8)sdlogger_1_irq_write_INTC_PRIOR_NUMBER);

    /* Enable it. */
    sdlogger_1_irq_write_Enable();
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_Stop
********************************************************************************
*
* Summary:
*   Disables and removes the interrupt.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_Stop(void)
{
    /* Disable this interrupt. */
    sdlogger_1_irq_write_Disable();

    /* Set the ISR to point to the passive one. */
    sdlogger_1_irq_write_SetVector(&IntDefaultHandler);
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_Interrupt
********************************************************************************
*
* Summary:
*   The default Interrupt Service Routine for sdlogger_1_irq_write.
*
*   Add custom code between the coments to keep the next version of this file
*   from over writting your code.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
CY_ISR(sdlogger_1_irq_write_Interrupt)
{
    /*  Place your Interrupt code here. */
    /* `#START sdlogger_1_irq_write_Interrupt` */

    /* `#END` */
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_SetVector
********************************************************************************
*
* Summary:
*   Change the ISR vector for the Interrupt. Note calling sdlogger_1_irq_write_Start
*   will override any effect this method would have had. To set the vector 
*   before the component has been started use sdlogger_1_irq_write_StartEx instead.
*
* Parameters:
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_SetVector(cyisraddress address)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    ramVectorTable[CYINT_IRQ_BASE + (uint32)sdlogger_1_irq_write__INTC_NUMBER] = address;
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_GetVector
********************************************************************************
*
* Summary:
*   Gets the "address" of the current ISR vector for the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Address of the ISR in the interrupt vector table.
*
*******************************************************************************/
cyisraddress sdlogger_1_irq_write_GetVector(void)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    return ramVectorTable[CYINT_IRQ_BASE + (uint32)sdlogger_1_irq_write__INTC_NUMBER];
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_SetPriority
********************************************************************************
*
* Summary:
*   Sets the Priority of the Interrupt. Note calling sdlogger_1_irq_write_Start
*   or sdlogger_1_irq_write_StartEx will override any effect this method 
*   would have had. This method should only be called after 
*   sdlogger_1_irq_write_Start or sdlogger_1_irq_write_StartEx has been called. To set 
*   the initial priority for the component use the cydwr file in the tool.
*
* Parameters:
*   priority: Priority of the interrupt. 0 - 7, 0 being the highest.
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_SetPriority(uint8 priority)
{
    *sdlogger_1_irq_write_INTC_PRIOR = priority << 5;
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_GetPriority
********************************************************************************
*
* Summary:
*   Gets the Priority of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Priority of the interrupt. 0 - 7, 0 being the highest.
*
*******************************************************************************/
uint8 sdlogger_1_irq_write_GetPriority(void)
{
    uint8 priority;


    priority = *sdlogger_1_irq_write_INTC_PRIOR >> 5;

    return priority;
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_Enable
********************************************************************************
*
* Summary:
*   Enables the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_Enable(void)
{
    /* Enable the general interrupt. */
    *sdlogger_1_irq_write_INTC_SET_EN = sdlogger_1_irq_write__INTC_MASK;
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_GetState
********************************************************************************
*
* Summary:
*   Gets the state (enabled, disabled) of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   1 if enabled, 0 if disabled.
*
*******************************************************************************/
uint8 sdlogger_1_irq_write_GetState(void)
{
    /* Get the state of the general interrupt. */
    return ((*sdlogger_1_irq_write_INTC_SET_EN & (uint32)sdlogger_1_irq_write__INTC_MASK) != 0u) ? 1u:0u;
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_Disable
********************************************************************************
*
* Summary:
*   Disables the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_Disable(void)
{
    /* Disable the general interrupt. */
    *sdlogger_1_irq_write_INTC_CLR_EN = sdlogger_1_irq_write__INTC_MASK;
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_SetPending
********************************************************************************
*
* Summary:
*   Causes the Interrupt to enter the pending state, a software method of
*   generating the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_SetPending(void)
{
    *sdlogger_1_irq_write_INTC_SET_PD = sdlogger_1_irq_write__INTC_MASK;
}


/*******************************************************************************
* Function Name: sdlogger_1_irq_write_ClearPending
********************************************************************************
*
* Summary:
*   Clears a pending interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void sdlogger_1_irq_write_ClearPending(void)
{
    *sdlogger_1_irq_write_INTC_CLR_PD = sdlogger_1_irq_write__INTC_MASK;
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
