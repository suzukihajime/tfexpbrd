/*******************************************************************************
* File Name: CTS_BT.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CTS_BT_H) /* Pins CTS_BT_H */
#define CY_PINS_CTS_BT_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "CTS_BT_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_80 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 CTS_BT__PORT == 15 && ((CTS_BT__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    CTS_BT_Write(uint8 value) ;
void    CTS_BT_SetDriveMode(uint8 mode) ;
uint8   CTS_BT_ReadDataReg(void) ;
uint8   CTS_BT_Read(void) ;
uint8   CTS_BT_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define CTS_BT_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define CTS_BT_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define CTS_BT_DM_RES_UP          PIN_DM_RES_UP
#define CTS_BT_DM_RES_DWN         PIN_DM_RES_DWN
#define CTS_BT_DM_OD_LO           PIN_DM_OD_LO
#define CTS_BT_DM_OD_HI           PIN_DM_OD_HI
#define CTS_BT_DM_STRONG          PIN_DM_STRONG
#define CTS_BT_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define CTS_BT_MASK               CTS_BT__MASK
#define CTS_BT_SHIFT              CTS_BT__SHIFT
#define CTS_BT_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define CTS_BT_PS                     (* (reg8 *) CTS_BT__PS)
/* Data Register */
#define CTS_BT_DR                     (* (reg8 *) CTS_BT__DR)
/* Port Number */
#define CTS_BT_PRT_NUM                (* (reg8 *) CTS_BT__PRT) 
/* Connect to Analog Globals */                                                  
#define CTS_BT_AG                     (* (reg8 *) CTS_BT__AG)                       
/* Analog MUX bux enable */
#define CTS_BT_AMUX                   (* (reg8 *) CTS_BT__AMUX) 
/* Bidirectional Enable */                                                        
#define CTS_BT_BIE                    (* (reg8 *) CTS_BT__BIE)
/* Bit-mask for Aliased Register Access */
#define CTS_BT_BIT_MASK               (* (reg8 *) CTS_BT__BIT_MASK)
/* Bypass Enable */
#define CTS_BT_BYP                    (* (reg8 *) CTS_BT__BYP)
/* Port wide control signals */                                                   
#define CTS_BT_CTL                    (* (reg8 *) CTS_BT__CTL)
/* Drive Modes */
#define CTS_BT_DM0                    (* (reg8 *) CTS_BT__DM0) 
#define CTS_BT_DM1                    (* (reg8 *) CTS_BT__DM1)
#define CTS_BT_DM2                    (* (reg8 *) CTS_BT__DM2) 
/* Input Buffer Disable Override */
#define CTS_BT_INP_DIS                (* (reg8 *) CTS_BT__INP_DIS)
/* LCD Common or Segment Drive */
#define CTS_BT_LCD_COM_SEG            (* (reg8 *) CTS_BT__LCD_COM_SEG)
/* Enable Segment LCD */
#define CTS_BT_LCD_EN                 (* (reg8 *) CTS_BT__LCD_EN)
/* Slew Rate Control */
#define CTS_BT_SLW                    (* (reg8 *) CTS_BT__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define CTS_BT_PRTDSI__CAPS_SEL       (* (reg8 *) CTS_BT__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define CTS_BT_PRTDSI__DBL_SYNC_IN    (* (reg8 *) CTS_BT__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define CTS_BT_PRTDSI__OE_SEL0        (* (reg8 *) CTS_BT__PRTDSI__OE_SEL0) 
#define CTS_BT_PRTDSI__OE_SEL1        (* (reg8 *) CTS_BT__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define CTS_BT_PRTDSI__OUT_SEL0       (* (reg8 *) CTS_BT__PRTDSI__OUT_SEL0) 
#define CTS_BT_PRTDSI__OUT_SEL1       (* (reg8 *) CTS_BT__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define CTS_BT_PRTDSI__SYNC_OUT       (* (reg8 *) CTS_BT__PRTDSI__SYNC_OUT) 


#if defined(CTS_BT__INTSTAT)  /* Interrupt Registers */

    #define CTS_BT_INTSTAT                (* (reg8 *) CTS_BT__INTSTAT)
    #define CTS_BT_SNAP                   (* (reg8 *) CTS_BT__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_CTS_BT_H */


/* [] END OF FILE */
