/*******************************************************************************
* File Name: btmodule_1_IRQ_RX.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_btmodule_1_IRQ_RX_H)
#define CY_ISR_btmodule_1_IRQ_RX_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void btmodule_1_IRQ_RX_Start(void);
void btmodule_1_IRQ_RX_StartEx(cyisraddress address);
void btmodule_1_IRQ_RX_Stop(void);

CY_ISR_PROTO(btmodule_1_IRQ_RX_Interrupt);

void btmodule_1_IRQ_RX_SetVector(cyisraddress address);
cyisraddress btmodule_1_IRQ_RX_GetVector(void);

void btmodule_1_IRQ_RX_SetPriority(uint8 priority);
uint8 btmodule_1_IRQ_RX_GetPriority(void);

void btmodule_1_IRQ_RX_Enable(void);
uint8 btmodule_1_IRQ_RX_GetState(void);
void btmodule_1_IRQ_RX_Disable(void);

void btmodule_1_IRQ_RX_SetPending(void);
void btmodule_1_IRQ_RX_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the btmodule_1_IRQ_RX ISR. */
#define btmodule_1_IRQ_RX_INTC_VECTOR            ((reg32 *) btmodule_1_IRQ_RX__INTC_VECT)

/* Address of the btmodule_1_IRQ_RX ISR priority. */
#define btmodule_1_IRQ_RX_INTC_PRIOR             ((reg8 *) btmodule_1_IRQ_RX__INTC_PRIOR_REG)

/* Priority of the btmodule_1_IRQ_RX interrupt. */
#define btmodule_1_IRQ_RX_INTC_PRIOR_NUMBER      btmodule_1_IRQ_RX__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable btmodule_1_IRQ_RX interrupt. */
#define btmodule_1_IRQ_RX_INTC_SET_EN            ((reg32 *) btmodule_1_IRQ_RX__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the btmodule_1_IRQ_RX interrupt. */
#define btmodule_1_IRQ_RX_INTC_CLR_EN            ((reg32 *) btmodule_1_IRQ_RX__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the btmodule_1_IRQ_RX interrupt state to pending. */
#define btmodule_1_IRQ_RX_INTC_SET_PD            ((reg32 *) btmodule_1_IRQ_RX__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the btmodule_1_IRQ_RX interrupt. */
#define btmodule_1_IRQ_RX_INTC_CLR_PD            ((reg32 *) btmodule_1_IRQ_RX__INTC_CLR_PD_REG)


#endif /* CY_ISR_btmodule_1_IRQ_RX_H */


/* [] END OF FILE */
