/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"
#include <string.h>

char `$INSTANCE_NAME`_stat;

CY_ISR(`$INSTANCE_NAME`_RX_ISR)
{
	char c;
	c = `$INSTANCE_NAME`_UART_ReadRxData();
	if(`$INSTANCE_NAME`_stat & `$INSTANCE_NAME`_MODE_COMM) {
		/* コマンドモードだったら */
		if(c == '\r') {
			if(strcmp(`$INSTANCE_NAME`_rxfifo_PopString(), `$INSTANCE_NAME`_STR_BYPASS_MODE) == 0) {
				`$INSTANCE_NAME`_stat &= ~`$INSTANCE_NAME`_MODE_COMM;
				`$INSTANCE_NAME`_stat |= `$INSTANCE_NAME`_MODE_BYPASS;
				UART_1_PutChar('!');
			}
			`$INSTANCE_NAME`_rxfifo_Clear(0);
		} else if(c != '\n') {
			`$INSTANCE_NAME`_rxfifo_PushChar(c);
		}
	} else {
		`$INSTANCE_NAME`_rxfifo_PushChar(c);
	}
	return;
}

CY_ISR(`$INSTANCE_NAME`_TX_ISR)
{
	if(`$INSTANCE_NAME`_txfifo_GetContentSize()) {
		`$INSTANCE_NAME`_UART_PutChar(`$INSTANCE_NAME`_txfifo_PopChar());
	}
	return;
}

void `$INSTANCE_NAME`_Start(void)
{
	int i;
	/* 変数初期化 */
	`$INSTANCE_NAME`_stat = `$INSTANCE_NAME`_MODE_COMM;
	/* 割り込み初期化 */
	`$INSTANCE_NAME`_IRQ_RX_StartEx(`$INSTANCE_NAME`_RX_ISR);
	`$INSTANCE_NAME`_IRQ_TX_StartEx(`$INSTANCE_NAME`_TX_ISR);
	/* UART初期化 */
	`$INSTANCE_NAME`_UART_Start();
	/* fifo初期化 */
	`$INSTANCE_NAME`_rxfifo_Start();
	`$INSTANCE_NAME`_txfifo_Start();
	/* BT moduleリセット */
	`$INSTANCE_NAME`_Control_Reg_Write(0);
	for(i = 0; i < 10000; i++) {}
	`$INSTANCE_NAME`_Control_Reg_Write(1);
	`$INSTANCE_NAME`_stat |= `$INSTANCE_NAME`_RUNNING;
	return;
}

void `$INSTANCE_NAME`_Stop(void)
{
	`$INSTANCE_NAME`_UART_Stop();
	`$INSTANCE_NAME`_Control_Reg_Write(0);
	`$INSTANCE_NAME`_stat &= ~`$INSTANCE_NAME`_RUNNING;
	return;
}

char `$INSTANCE_NAME`_GetChar(void)
{
	return(`$INSTANCE_NAME`_rxfifo_PopChar());
}

char *`$INSTANCE_NAME`_GetString(void)
{
	return(`$INSTANCE_NAME`_rxfifo_PopString());
}

void `$INSTANCE_NAME`_PutChar(char c)
{
	`$INSTANCE_NAME`_UART_PutChar(c);
	return;
}

void `$INSTANCE_NAME`_PutString(char *str)
{
	while(*str != '\0') {
		`$INSTANCE_NAME`_txfifo_PushChar(*str++);
	}
	if(`$INSTANCE_NAME`_txfifo_GetContentSize()) {
		`$INSTANCE_NAME`_UART_PutChar(`$INSTANCE_NAME`_txfifo_PopChar());
	}
	return;
}

char `$INSTANCE_NAME`_GetStatus(void)
{
	if(`$INSTANCE_NAME`_txfifo_GetContentSize()) {
		`$INSTANCE_NAME`_stat |= `$INSTANCE_NAME`_TXFIFO_NEMPTY;
	} else {
		`$INSTANCE_NAME`_stat &= ~`$INSTANCE_NAME`_TXFIFO_NEMPTY;
	}
	if(`$INSTANCE_NAME`_rxfifo_GetContentSize()) {
		`$INSTANCE_NAME`_stat |= `$INSTANCE_NAME`_RXFIFO_NEMPTY;
	} else {
		`$INSTANCE_NAME`_stat &= ~`$INSTANCE_NAME`_RXFIFO_NEMPTY;
	}
	return(`$INSTANCE_NAME`_stat);
}

/* [] END OF FILE */
