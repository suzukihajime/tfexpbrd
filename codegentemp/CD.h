/*******************************************************************************
* File Name: CD.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CD_H) /* Pins CD_H */
#define CY_PINS_CD_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "CD_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_80 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 CD__PORT == 15 && ((CD__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    CD_Write(uint8 value) ;
void    CD_SetDriveMode(uint8 mode) ;
uint8   CD_ReadDataReg(void) ;
uint8   CD_Read(void) ;
uint8   CD_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define CD_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define CD_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define CD_DM_RES_UP          PIN_DM_RES_UP
#define CD_DM_RES_DWN         PIN_DM_RES_DWN
#define CD_DM_OD_LO           PIN_DM_OD_LO
#define CD_DM_OD_HI           PIN_DM_OD_HI
#define CD_DM_STRONG          PIN_DM_STRONG
#define CD_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define CD_MASK               CD__MASK
#define CD_SHIFT              CD__SHIFT
#define CD_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define CD_PS                     (* (reg8 *) CD__PS)
/* Data Register */
#define CD_DR                     (* (reg8 *) CD__DR)
/* Port Number */
#define CD_PRT_NUM                (* (reg8 *) CD__PRT) 
/* Connect to Analog Globals */                                                  
#define CD_AG                     (* (reg8 *) CD__AG)                       
/* Analog MUX bux enable */
#define CD_AMUX                   (* (reg8 *) CD__AMUX) 
/* Bidirectional Enable */                                                        
#define CD_BIE                    (* (reg8 *) CD__BIE)
/* Bit-mask for Aliased Register Access */
#define CD_BIT_MASK               (* (reg8 *) CD__BIT_MASK)
/* Bypass Enable */
#define CD_BYP                    (* (reg8 *) CD__BYP)
/* Port wide control signals */                                                   
#define CD_CTL                    (* (reg8 *) CD__CTL)
/* Drive Modes */
#define CD_DM0                    (* (reg8 *) CD__DM0) 
#define CD_DM1                    (* (reg8 *) CD__DM1)
#define CD_DM2                    (* (reg8 *) CD__DM2) 
/* Input Buffer Disable Override */
#define CD_INP_DIS                (* (reg8 *) CD__INP_DIS)
/* LCD Common or Segment Drive */
#define CD_LCD_COM_SEG            (* (reg8 *) CD__LCD_COM_SEG)
/* Enable Segment LCD */
#define CD_LCD_EN                 (* (reg8 *) CD__LCD_EN)
/* Slew Rate Control */
#define CD_SLW                    (* (reg8 *) CD__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define CD_PRTDSI__CAPS_SEL       (* (reg8 *) CD__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define CD_PRTDSI__DBL_SYNC_IN    (* (reg8 *) CD__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define CD_PRTDSI__OE_SEL0        (* (reg8 *) CD__PRTDSI__OE_SEL0) 
#define CD_PRTDSI__OE_SEL1        (* (reg8 *) CD__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define CD_PRTDSI__OUT_SEL0       (* (reg8 *) CD__PRTDSI__OUT_SEL0) 
#define CD_PRTDSI__OUT_SEL1       (* (reg8 *) CD__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define CD_PRTDSI__SYNC_OUT       (* (reg8 *) CD__PRTDSI__SYNC_OUT) 


#if defined(CD__INTSTAT)  /* Interrupt Registers */

    #define CD_INTSTAT                (* (reg8 *) CD__INTSTAT)
    #define CD_SNAP                   (* (reg8 *) CD__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_CD_H */


/* [] END OF FILE */
