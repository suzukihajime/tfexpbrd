/*******************************************************************************
* File Name: btmodule_1_UART_PM.c
* Version 2.30
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "btmodule_1_UART.h"


/***************************************
* Local data allocation
***************************************/

static btmodule_1_UART_BACKUP_STRUCT  btmodule_1_UART_backup =
{
    /* enableState - disabled */
    0u,
};



/*******************************************************************************
* Function Name: btmodule_1_UART_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  btmodule_1_UART_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void btmodule_1_UART_SaveConfig(void)
{
    #if (CY_UDB_V0)

        #if(btmodule_1_UART_CONTROL_REG_REMOVED == 0u)
            btmodule_1_UART_backup.cr = btmodule_1_UART_CONTROL_REG;
        #endif /* End btmodule_1_UART_CONTROL_REG_REMOVED */

        #if( (btmodule_1_UART_RX_ENABLED) || (btmodule_1_UART_HD_ENABLED) )
            btmodule_1_UART_backup.rx_period = btmodule_1_UART_RXBITCTR_PERIOD_REG;
            btmodule_1_UART_backup.rx_mask = btmodule_1_UART_RXSTATUS_MASK_REG;
            #if (btmodule_1_UART_RXHW_ADDRESS_ENABLED)
                btmodule_1_UART_backup.rx_addr1 = btmodule_1_UART_RXADDRESS1_REG;
                btmodule_1_UART_backup.rx_addr2 = btmodule_1_UART_RXADDRESS2_REG;
            #endif /* End btmodule_1_UART_RXHW_ADDRESS_ENABLED */
        #endif /* End btmodule_1_UART_RX_ENABLED | btmodule_1_UART_HD_ENABLED*/

        #if(btmodule_1_UART_TX_ENABLED)
            #if(btmodule_1_UART_TXCLKGEN_DP)
                btmodule_1_UART_backup.tx_clk_ctr = btmodule_1_UART_TXBITCLKGEN_CTR_REG;
                btmodule_1_UART_backup.tx_clk_compl = btmodule_1_UART_TXBITCLKTX_COMPLETE_REG;
            #else
                btmodule_1_UART_backup.tx_period = btmodule_1_UART_TXBITCTR_PERIOD_REG;
            #endif /*End btmodule_1_UART_TXCLKGEN_DP */
            btmodule_1_UART_backup.tx_mask = btmodule_1_UART_TXSTATUS_MASK_REG;
        #endif /*End btmodule_1_UART_TX_ENABLED */


    #else /* CY_UDB_V1 */

        #if(btmodule_1_UART_CONTROL_REG_REMOVED == 0u)
            btmodule_1_UART_backup.cr = btmodule_1_UART_CONTROL_REG;
        #endif /* End btmodule_1_UART_CONTROL_REG_REMOVED */

    #endif  /* End CY_UDB_V0 */
}


/*******************************************************************************
* Function Name: btmodule_1_UART_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  btmodule_1_UART_backup - used when non-retention registers are restored.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void btmodule_1_UART_RestoreConfig(void)
{

    #if (CY_UDB_V0)

        #if(btmodule_1_UART_CONTROL_REG_REMOVED == 0u)
            btmodule_1_UART_CONTROL_REG = btmodule_1_UART_backup.cr;
        #endif /* End btmodule_1_UART_CONTROL_REG_REMOVED */

        #if( (btmodule_1_UART_RX_ENABLED) || (btmodule_1_UART_HD_ENABLED) )
            btmodule_1_UART_RXBITCTR_PERIOD_REG = btmodule_1_UART_backup.rx_period;
            btmodule_1_UART_RXSTATUS_MASK_REG = btmodule_1_UART_backup.rx_mask;
            #if (btmodule_1_UART_RXHW_ADDRESS_ENABLED)
                btmodule_1_UART_RXADDRESS1_REG = btmodule_1_UART_backup.rx_addr1;
                btmodule_1_UART_RXADDRESS2_REG = btmodule_1_UART_backup.rx_addr2;
            #endif /* End btmodule_1_UART_RXHW_ADDRESS_ENABLED */
        #endif  /* End (btmodule_1_UART_RX_ENABLED) || (btmodule_1_UART_HD_ENABLED) */

        #if(btmodule_1_UART_TX_ENABLED)
            #if(btmodule_1_UART_TXCLKGEN_DP)
                btmodule_1_UART_TXBITCLKGEN_CTR_REG = btmodule_1_UART_backup.tx_clk_ctr;
                btmodule_1_UART_TXBITCLKTX_COMPLETE_REG = btmodule_1_UART_backup.tx_clk_compl;
            #else
                btmodule_1_UART_TXBITCTR_PERIOD_REG = btmodule_1_UART_backup.tx_period;
            #endif /*End btmodule_1_UART_TXCLKGEN_DP */
            btmodule_1_UART_TXSTATUS_MASK_REG = btmodule_1_UART_backup.tx_mask;
        #endif /*End btmodule_1_UART_TX_ENABLED */

    #else /* CY_UDB_V1 */

        #if(btmodule_1_UART_CONTROL_REG_REMOVED == 0u)
            btmodule_1_UART_CONTROL_REG = btmodule_1_UART_backup.cr;
        #endif /* End btmodule_1_UART_CONTROL_REG_REMOVED */

    #endif  /* End CY_UDB_V0 */
}


/*******************************************************************************
* Function Name: btmodule_1_UART_Sleep
********************************************************************************
*
* Summary:
*  Stops and saves the user configuration. Should be called
*  just prior to entering sleep.
*
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  btmodule_1_UART_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void btmodule_1_UART_Sleep(void)
{

    #if(btmodule_1_UART_RX_ENABLED || btmodule_1_UART_HD_ENABLED)
        if((btmodule_1_UART_RXSTATUS_ACTL_REG  & btmodule_1_UART_INT_ENABLE) != 0u)
        {
            btmodule_1_UART_backup.enableState = 1u;
        }
        else
        {
            btmodule_1_UART_backup.enableState = 0u;
        }
    #else
        if((btmodule_1_UART_TXSTATUS_ACTL_REG  & btmodule_1_UART_INT_ENABLE) !=0u)
        {
            btmodule_1_UART_backup.enableState = 1u;
        }
        else
        {
            btmodule_1_UART_backup.enableState = 0u;
        }
    #endif /* End btmodule_1_UART_RX_ENABLED || btmodule_1_UART_HD_ENABLED*/

    btmodule_1_UART_Stop();
    btmodule_1_UART_SaveConfig();
}


/*******************************************************************************
* Function Name: btmodule_1_UART_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called
*  just after awaking from sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  btmodule_1_UART_backup - used when non-retention registers are restored.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void btmodule_1_UART_Wakeup(void)
{
    btmodule_1_UART_RestoreConfig();
    #if( (btmodule_1_UART_RX_ENABLED) || (btmodule_1_UART_HD_ENABLED) )
        btmodule_1_UART_ClearRxBuffer();
    #endif /* End (btmodule_1_UART_RX_ENABLED) || (btmodule_1_UART_HD_ENABLED) */
    #if(btmodule_1_UART_TX_ENABLED || btmodule_1_UART_HD_ENABLED)
        btmodule_1_UART_ClearTxBuffer();
    #endif /* End btmodule_1_UART_TX_ENABLED || btmodule_1_UART_HD_ENABLED */

    if(btmodule_1_UART_backup.enableState != 0u)
    {
        btmodule_1_UART_Enable();
    }
}


/* [] END OF FILE */
