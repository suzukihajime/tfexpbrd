/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

/* DMA Configuration for DMA_Buzzer */
#define `$INSTANCE_NAME`_DMA_BYTES_PER_BURST 1
#define `$INSTANCE_NAME`_DMA_REQUEST_PER_BURST 1
#define `$INSTANCE_NAME`_DMA_SRC_BASE (CYDEV_SRAM_BASE)
#define `$INSTANCE_NAME`_DMA_DST_BASE (CYDEV_PERIPH_BASE)

#define	`$INSTANCE_NAME`_CONTINUOUS		(1)
#define	`$INSTANCE_NAME`_ONESHOT		(0)


void `$INSTANCE_NAME`_Start(void);
void `$INSTANCE_NAME`_Run(uint8, uint8);
void `$INSTANCE_NAME`_Stop(void);
//[] END OF FILE
