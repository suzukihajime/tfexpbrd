/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

typedef enum {
	`$INSTANCE_NAME`_OK = 0,
	`$INSTANCE_NAME`_NODISK,
	`$INSTANCE_NAME`_ERROR
} `$INSTANCE_NAME`_RESULT;

#define	`$INSTANCE_NAME`_AVAILABLE		0x01			// SDが挿入されている
#define	`$INSTANCE_NAME`_LOGGING		0x02			// ログ取得中

#define	`$INSTANCE_NAME`_WBUF_LENGTH	4096			// 4096バイトたまったら書き込み

#define	`$INSTANCE_NAME`_FILENAME		"LOG%03d.DAT"	// そのままsprintfに渡す

int `$INSTANCE_NAME`_Start(void);
int `$INSTANCE_NAME`_Stop(void);
int `$INSTANCE_NAME`_CreateLog(void);
int `$INSTANCE_NAME`_CloseLog(void);
/* 最低限の書き込み関数 */
int `$INSTANCE_NAME`_Write(char *str, int len);
int `$INSTANCE_NAME`_WriteLine(char *str, int len);
/* sylphide形式のデータを書く関数を作るといいと思うが、sylphide形式はよく分からない */


/* status取得 */
int `$INSTANCE_NAME`_GetStatus(void);
void `$INSTANCE_NAME`_UpdateStatus(void);

/* ファイル名取得 */
char *`$INSTANCE_NAME`_GetFileName(void);
char `$INSTANCE_NAME`_Find(char *filename);

//[] END OF FILE
