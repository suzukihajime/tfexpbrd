/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

#if !defined(`$INSTANCE_NAME`_H)
#define	`$INSTANCE_NAME`_H

void sdspi_start(void);
void sdspi_stop(void);

#include "`$INSTANCE_NAME`_diskio.h"

#endif	/* end of #if !defined(`$INSTANCE_NAME`_H) */

//[] END OF FILE
