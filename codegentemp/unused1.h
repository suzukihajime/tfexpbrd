/*******************************************************************************
* File Name: unused1.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_unused1_H) /* Pins unused1_H */
#define CY_PINS_unused1_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "unused1_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_80 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 unused1__PORT == 15 && ((unused1__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    unused1_Write(uint8 value) ;
void    unused1_SetDriveMode(uint8 mode) ;
uint8   unused1_ReadDataReg(void) ;
uint8   unused1_Read(void) ;
uint8   unused1_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define unused1_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define unused1_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define unused1_DM_RES_UP          PIN_DM_RES_UP
#define unused1_DM_RES_DWN         PIN_DM_RES_DWN
#define unused1_DM_OD_LO           PIN_DM_OD_LO
#define unused1_DM_OD_HI           PIN_DM_OD_HI
#define unused1_DM_STRONG          PIN_DM_STRONG
#define unused1_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define unused1_MASK               unused1__MASK
#define unused1_SHIFT              unused1__SHIFT
#define unused1_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define unused1_PS                     (* (reg8 *) unused1__PS)
/* Data Register */
#define unused1_DR                     (* (reg8 *) unused1__DR)
/* Port Number */
#define unused1_PRT_NUM                (* (reg8 *) unused1__PRT) 
/* Connect to Analog Globals */                                                  
#define unused1_AG                     (* (reg8 *) unused1__AG)                       
/* Analog MUX bux enable */
#define unused1_AMUX                   (* (reg8 *) unused1__AMUX) 
/* Bidirectional Enable */                                                        
#define unused1_BIE                    (* (reg8 *) unused1__BIE)
/* Bit-mask for Aliased Register Access */
#define unused1_BIT_MASK               (* (reg8 *) unused1__BIT_MASK)
/* Bypass Enable */
#define unused1_BYP                    (* (reg8 *) unused1__BYP)
/* Port wide control signals */                                                   
#define unused1_CTL                    (* (reg8 *) unused1__CTL)
/* Drive Modes */
#define unused1_DM0                    (* (reg8 *) unused1__DM0) 
#define unused1_DM1                    (* (reg8 *) unused1__DM1)
#define unused1_DM2                    (* (reg8 *) unused1__DM2) 
/* Input Buffer Disable Override */
#define unused1_INP_DIS                (* (reg8 *) unused1__INP_DIS)
/* LCD Common or Segment Drive */
#define unused1_LCD_COM_SEG            (* (reg8 *) unused1__LCD_COM_SEG)
/* Enable Segment LCD */
#define unused1_LCD_EN                 (* (reg8 *) unused1__LCD_EN)
/* Slew Rate Control */
#define unused1_SLW                    (* (reg8 *) unused1__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define unused1_PRTDSI__CAPS_SEL       (* (reg8 *) unused1__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define unused1_PRTDSI__DBL_SYNC_IN    (* (reg8 *) unused1__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define unused1_PRTDSI__OE_SEL0        (* (reg8 *) unused1__PRTDSI__OE_SEL0) 
#define unused1_PRTDSI__OE_SEL1        (* (reg8 *) unused1__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define unused1_PRTDSI__OUT_SEL0       (* (reg8 *) unused1__PRTDSI__OUT_SEL0) 
#define unused1_PRTDSI__OUT_SEL1       (* (reg8 *) unused1__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define unused1_PRTDSI__SYNC_OUT       (* (reg8 *) unused1__PRTDSI__SYNC_OUT) 


#if defined(unused1__INTSTAT)  /* Interrupt Registers */

    #define unused1_INTSTAT                (* (reg8 *) unused1__INTSTAT)
    #define unused1_SNAP                   (* (reg8 *) unused1__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_unused1_H */


/* [] END OF FILE */
