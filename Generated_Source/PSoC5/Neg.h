/*******************************************************************************
* File Name: Neg.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Neg_H) /* Pins Neg_H */
#define CY_PINS_Neg_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Neg_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_80 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Neg__PORT == 15 && ((Neg__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    Neg_Write(uint8 value) ;
void    Neg_SetDriveMode(uint8 mode) ;
uint8   Neg_ReadDataReg(void) ;
uint8   Neg_Read(void) ;
uint8   Neg_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Neg_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Neg_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Neg_DM_RES_UP          PIN_DM_RES_UP
#define Neg_DM_RES_DWN         PIN_DM_RES_DWN
#define Neg_DM_OD_LO           PIN_DM_OD_LO
#define Neg_DM_OD_HI           PIN_DM_OD_HI
#define Neg_DM_STRONG          PIN_DM_STRONG
#define Neg_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Neg_MASK               Neg__MASK
#define Neg_SHIFT              Neg__SHIFT
#define Neg_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Neg_PS                     (* (reg8 *) Neg__PS)
/* Data Register */
#define Neg_DR                     (* (reg8 *) Neg__DR)
/* Port Number */
#define Neg_PRT_NUM                (* (reg8 *) Neg__PRT) 
/* Connect to Analog Globals */                                                  
#define Neg_AG                     (* (reg8 *) Neg__AG)                       
/* Analog MUX bux enable */
#define Neg_AMUX                   (* (reg8 *) Neg__AMUX) 
/* Bidirectional Enable */                                                        
#define Neg_BIE                    (* (reg8 *) Neg__BIE)
/* Bit-mask for Aliased Register Access */
#define Neg_BIT_MASK               (* (reg8 *) Neg__BIT_MASK)
/* Bypass Enable */
#define Neg_BYP                    (* (reg8 *) Neg__BYP)
/* Port wide control signals */                                                   
#define Neg_CTL                    (* (reg8 *) Neg__CTL)
/* Drive Modes */
#define Neg_DM0                    (* (reg8 *) Neg__DM0) 
#define Neg_DM1                    (* (reg8 *) Neg__DM1)
#define Neg_DM2                    (* (reg8 *) Neg__DM2) 
/* Input Buffer Disable Override */
#define Neg_INP_DIS                (* (reg8 *) Neg__INP_DIS)
/* LCD Common or Segment Drive */
#define Neg_LCD_COM_SEG            (* (reg8 *) Neg__LCD_COM_SEG)
/* Enable Segment LCD */
#define Neg_LCD_EN                 (* (reg8 *) Neg__LCD_EN)
/* Slew Rate Control */
#define Neg_SLW                    (* (reg8 *) Neg__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Neg_PRTDSI__CAPS_SEL       (* (reg8 *) Neg__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Neg_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Neg__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Neg_PRTDSI__OE_SEL0        (* (reg8 *) Neg__PRTDSI__OE_SEL0) 
#define Neg_PRTDSI__OE_SEL1        (* (reg8 *) Neg__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Neg_PRTDSI__OUT_SEL0       (* (reg8 *) Neg__PRTDSI__OUT_SEL0) 
#define Neg_PRTDSI__OUT_SEL1       (* (reg8 *) Neg__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Neg_PRTDSI__SYNC_OUT       (* (reg8 *) Neg__PRTDSI__SYNC_OUT) 


#if defined(Neg__INTSTAT)  /* Interrupt Registers */

    #define Neg_INTSTAT                (* (reg8 *) Neg__INTSTAT)
    #define Neg_SNAP                   (* (reg8 *) Neg__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Neg_H */


/* [] END OF FILE */
