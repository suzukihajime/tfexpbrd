/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

#define	btmodule_1_STR_BYPASS_MODE	"AT-AB -BypassMode-"
#define	btmodule_1_STR_COMMAND_MODE	"AT-AB -CommandMode-"

#define	btmodule_1_RUNNING			(1)
#define	btmodule_1_MODE_COMM			(2)
#define	btmodule_1_MODE_BYPASS		(4)
#define	btmodule_1_TXFIFO_NEMPTY		(8)
#define	btmodule_1_RXFIFO_NEMPTY		(16)

void btmodule_1_RX_ISR(void);
void btmodule_1_TX_ISR(void);
void btmodule_1_Start(void);
void btmodule_1_Stop(void);
void btmodule_1_PutChar(char);
void btmodule_1_PutString(char*);
char btmodule_1_GetChar(void);
char *btmodule_1_GetString(void);
char btmodule_1_GetStatus(void);

//[] END OF FILE
